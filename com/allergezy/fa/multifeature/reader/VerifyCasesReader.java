package com.allergezy.fa.multifeature.reader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Singleton class to read in the cases for the Verify cases
 * setup/summary.txt -- define the cases
 * 
 * Each case is a combination of cpg to be used
 * 
 * @author
 *
 */
public class VerifyCasesReader implements FoodAllergyConstants {
	private static int SINGLE_FEATURE_ID_BOOST = 100000;
	private static VerifyCasesReader instance = null;
	private List<List<String>> allCases = null;
	private Map<Integer, List<String>> casesMap = null;

	//Private constructor for Singleton
	private VerifyCasesReader() {
		initialize();
	}

	//Get instance method 
	public static VerifyCasesReader getInstance() {
		if (instance == null) {
			synchronized (VerifyCasesReader.class) {
				instance = new VerifyCasesReader();
			}
		}
		return instance;
	}

	//Read the contents of the file and cache the information
	private void initialize() {
		//gse59999/verify/setup/summary.txt -- define the cases
		String fileName = GSE59999_Verify_Cases_InputFileName;
		this.allCases = new ArrayList<List<String>>();
		
		String [] rows = InMemoryFileReader.getFileContentByRows(fileName);
		for (String row: rows) {
			String [] terms = row.split("\t");
			if (isValidRow(terms) ) {
				ArrayList<String> eg = new ArrayList<String>();
				this.allCases.add(eg);
				eg.add("" + Integer.parseInt(terms[0]));
				for (int i = 1; i < terms.length; i ++) {
					if (terms[i].startsWith("cg")) {
						eg.add(terms[i]);
					}
				}
			}
		}
		cleanUpIds();
	}
	
	//Assign unique ids for the cases
	private void cleanUpIds() {
		this.casesMap = new HashMap<Integer, List<String>>();
		for (List<String> example: this.allCases) {
			if (example.size() == 2) {
				int oldId = Integer.parseInt(example.get(0));
				if (this.casesMap.containsKey(oldId)) {
				//First is id
					int id = SINGLE_FEATURE_ID_BOOST + Integer.parseInt(example.get(0));
					example.set(0, "" + id);
				}
			}
			Integer id = Integer.parseInt(example.get(0));
			this.casesMap.put(id, example);
		}
	}
	
	//Get a particular example
	public List<String> getExampleById(int id) {
		return this.casesMap.get(id);
	}
	
	//Get all examples
	public List<List<String>> getExamples() {
		return this.allCases;
	}
	
	//Get the example specified by the caseNumber
	public List<String> getExample(int caseNumber) {
		return this.allCases.get(caseNumber);
	}
	
	//Check if the row is valid in the file
	private boolean isValidRow(String [] terms) {
		if (terms.length > 1) {
			try {
				double d = Double.parseDouble(terms[0]);
				return true;
			} catch (Throwable th) {
				return false;
			}
		}
		return false;
	}
	
	//Main method
	public static void main(String [] args) {
		VerifyCasesReader cv = VerifyCasesReader.getInstance();
		List<List<String>> egs = cv.getExamples();
		for (List<String> eg: egs) {
			for (String t: eg) {
				System.out.print(t + ", " );
			}
			System.out.println();
		}
	}
}
