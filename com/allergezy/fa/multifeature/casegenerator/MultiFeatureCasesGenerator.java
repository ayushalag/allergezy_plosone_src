package com.allergezy.fa.multifeature.casegenerator;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Map;

import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * This class generates the combinations of cases that need to be tried for a 
 * given number of inputs.
 * @author Ayush Alag
 *
 */
public class MultiFeatureCasesGenerator implements FoodAllergyConstants {

	public void generateThreeCases(String [] cpgs, String outputFileName) throws Exception {
		//Three at a time
		String [] b1 = {"cg06410630", "cg24616138"};
		String [] b2 = {"cg06410630","cg00939931"};
		String [] b3 = {"cg06628000","cg19287711"};
		String [] b4 = {"cg18988685","cg27027230"};
		String [] b5 = {"cg06410630","cg27027230"};
		String [] b6 = {"cg06410630","cg10461264"};
		String [] b7 = {"cg06410630","cg06116095"};
		String [] b8 = {"cg06410630","cg03068039"};
		String [] b9 = {"cg18988685","cg00939931"};
		String [][] b = {b1, b2, b3, b4, b5, b6, b7, b8, b9};
		int startId = 1001;
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateFourCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg26963090"};
		String [] b2 = {"cg06410630","cg10461264","cg06116095"};
		String [] b3 = {"cg06410630","cg10461264","cg19287711"};
		String [] b4 = {"cg06410630","cg06116095","cg10461264"};
		String [] b5 = {"cg06410630","cg10461264","cg06628000"};
		String [] b6 = {"cg06410630","cg10461264","cg00936790"};
		String [] b7 = {"cg06410630","cg10461264","cg25890092"};
		String [] b8 = {"cg06410630","cg10461264","cg24616138"};
		String [] b9 = {"cg06410630","cg10461264","cg27027230"};
		
		String [] b10 = {"cg06410630","cg24616138","cg10461264"};
		String [] b11 = {"cg06410630","cg24616138","cg19287711"};
		String [] b12 = {"cg06410630","cg00939931","cg18988685"};
		String [] b13 = {"cg06410630","cg27027230","cg10461264"};
		String [] b14 = {"cg06410630","cg27027230","cg24616138"};
		String [] b15 = {"cg18988685","cg00939931","cg07060505"};

		String [][] b = {b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15};
		int startId = 10001;
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateFiveCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg25890092","cg06116095"};
		String [] b2 = {"cg06410630","cg10461264","cg26963090","cg27027230"};
		String [] b3 = {"cg06410630","cg10461264","cg06116095","cg07033513"};
		String [] b4 = {"cg06410630","cg06116095","cg10461264","cg07033513"};
		String [] b5 = {"cg06410630","cg10461264","cg27027230","cg06116095"};
		String [] b6 = {"cg06410630","cg10461264","cg26963090","cg00936790"};
		String [] b7 = {"cg06410630","cg10461264","cg06116095","cg25890092"};
		String [] b8 = {"cg06410630","cg10461264","cg19287711","cg06628000"};
		String [] b9 = {"cg06410630","cg10461264","cg10461264","cg25890092"};
		
		String [] b10 = {"cg06410630","cg10461264","cg06628000","cg19287711"};
		String [] b11 = {"cg06410630","cg10461264","cg06116095","cg06628000"};
		String [] b12 = {"cg06410630","cg06116095","cg10461264","cg06628000"};
		String [] b13 = {"cg06410630","cg10461264","cg06628000","cg06116095"};
		String [] b14 = {"cg06410630","cg10461264","cg06628000","cg25890092"};
		String [] b15 = {"cg06410630","cg10461264","cg25890092","cg06628000"};
		
		String [] b16 = {"cg06410630","cg10461264","cg06628000","cg00939931"};
		String [] b17 = {"cg18988685","cg00939931","cg07060505","cg10461264"};

		String [][] b = {b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15,b16,b17};
		int startId = 100001;
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateSixCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090"};
		String [] b2 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg18988685"};
		String [] b3 = {"cg06410630","cg10461264","cg25890092","cg06628000","cg26963090"};
		String [] b5 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg25890092"};
		String [] b6 = {"cg06410630","cg10461264","cg06628000","cg25890092","cg18988685"};
		String [] b8 = {"cg06410630","cg10461264","cg06116095","cg07033513","cg06628000"};
		String [] b9 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg07033513"};
		
		String [] b10 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630"};
		String [] b11 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg25890092"};
		String [] b12 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg07060505"};
		String [] b13 = {"cg06410630","cg10461264","cg06116095","cg07033513","cg00936790"};

		String [][] b = {b1, b2, b3,  b5, b6, b8, b9, b10, b11, b12, b13};
		int startId = 1000001;
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateSevenCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090","cg18988685"};
		String [] b2 = {"cg06410630","cg10461264","cg06628000","cg25890092","cg26963090","cg14414100"};
		String [] b3 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg25890092"};
		String [] b4 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg25890092","cg24616138"};
		String [] b5 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000"};
		String [] b6 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg25890092","cg00936790"};
		String [] b7 = {"cg06410630","cg10461264","cg06628000","cg25890092","cg18988685","cg00939931"};
		String [] b8 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg06669701"};
		String [] b9 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg19287711"};
		
		String [] b10 = {"cg06410630","cg10461264","cg25890092","cg06628000","cg26963090","cg06669701"};
		String [] b11 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg25890092", "cg19287711"};
		String [] b12 = {"cg06410630","cg10461264","cg06628000","cg25890092","cg26963090","cg27027230"};
		String [] b13 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg07060505","cg00936790"};
		String [] b14 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg27027230"};
		String [] b15 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg07033513","cg00936790"};
		String [] b16 = {"cg06410630","cg10461264","cg26963090","cg27027230","cg25890092","cg14414100"};
		
		String [][] b = {b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13,b14,b15,b16};
		int startId = 10000001;
		int index = 1;
		for (String [] l: b) {
			int inner = 1;
			for (String [] i: b) {
				if (index != inner) {
					boolean flag = isSame(i,l);
					if (flag) {
						System.out.println("Found same: " + index + " -> " + inner);
					}
				}
				inner ++;
			}
			index ++;
		}
		generateCases(b,cpgs,startId,outputFileName);
	}



	public void generateEightCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266"};
		String [] b2 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039"};
		String [] b3 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg07033513"};
	
		String [] b5 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg06669701","cg18988685"};
		String [] b6 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg19287711","cg02788266"};
		String [] b7 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg19287711","cg18988685"};
		String [] b8 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg06669701","cg27027230"};
		String [] b9 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg06669701"};
		
		String [] b10 = {"cg06410630","cg06116095","cg10461264","cg06628000","cg25890092","cg00936790","cg26963090"};
		
		String [][] b = {b1, b2, b3,  b5, b6, b7, b8, b9, b10};
		int startId = 100000001;
		int index = 1;
		for (String [] l: b) {
			int inner = 1;
			for (String [] i: b) {
				if (index != inner) {
					boolean flag = isSame(i,l);
					if (flag) {
						System.out.println("Found same: " + index + " -> " + inner);
					}
				}
				inner ++;
			}
			index ++;
		}
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateNineCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090","cg06669701","cg18988685","cg03068039"};
		String [] b2 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092"};
		String [] b3 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg24616138"};
	
		String [] b5 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg00936790"};
	//	String [] b6 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg25890092"};
		String [] b7 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg06669701","cg03068039"};
	
	//	String [] b8 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg06669701"};
	//	String [] b9 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg07033513"};
		
		String [] b10 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg07033513","cg03068039"};
		String [] b11 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039"};
		
		String [][] b = {b1, b2, b3,  b5, b7, b10, b11};
		int startId = 100000001;
		int index = 1;
		for (String [] l: b) {
			int inner = 1;
			for (String [] i: b) {
				if (index != inner) {
					boolean flag = isSame(i,l);
					if (flag) {
						System.out.println("Found same: " + index + " -> " + inner);
					}
				}
				inner ++;
			}
			index ++;
		}
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateTenCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711"};
		String [] b2 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg27027230"};
		String [] b3 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg25890092"};
	
		String [] b5 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711"};
		String [] b6 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg06669701","cg03068039","cg07033513"};
		String [] b7 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg24616138","cg25890092"};
	
	//	String [] b8 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg07033513","cg03068039","cg00936790"};
    	String [] b9 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg06669701","cg03068039","cg07033513"};
		
	//	String [] b10 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg07033513","cg03068039","cg06669701"};
		String [] b11 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg00936790","cg07033513"};
		
		String [][] b = {b1, b2, b3,  b5, b6, b7, b9, b11};
		int startId = 1000000001;
		checkForDuplicates(b);
		generateCases(b,cpgs,startId,outputFileName);
	}


	public void generateElevenCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg06410630", "cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg24616138"};
		String [] b2 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg14414100"};
		String [] b3 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg27027230","cg06116095"};
	
		String [] b5 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg02788266"};
		String [] b6 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg25890092","cg00939931"};
		String [] b7 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg25890092","cg19287711"};
	
		String [] b8 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg07060505"};

    	String [] b9 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg06669701","cg03068039","cg07033513"};
		
	//	String [] b10 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg07033513","cg03068039","cg06669701"};
		String [] b11 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg00936790","cg07033513"};
		
		String [][] b = {b1, b2, b3, b5, b6, b7,b8};
		int startId = 1000000001;
		checkForDuplicates(b);
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	public void generateTwelveCases(String [] cpgs, String outputFileName) throws Exception {
		String [] b1 = {"cg18988685", "cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg14414100","cg26963090"};
		String [] b2 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg02788266","cg14414100"};
		String [] b3 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg02788266","cg26963090"};
		String [] b4 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg14414100","cg07033513"};		
		String [] b5 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg02788266","cg07033513"};	
		String [] b6 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg07060505","cg00939931"};	
		String [] b7 = {"cg18988685", "cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg19287711","cg02788266","cg24616138"};
		String [] b8 = {"cg18988685", "cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg27027230","cg06116095","cg00936790"};
    	String [] b9 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg24616138","cg00936790"};
		
		String [] b10 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg24616138","cg00939931"};
		String [] b11 = {"cg18988685","cg00939931","cg07060505","cg10461264","cg06410630","cg06628000","cg03068039","cg25890092","cg27027230","cg06116095","cg07033513"};
		String [] b12 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg19287711","cg24616138","cg07033513"};
		String [] b13 = {"cg06410630","cg10461264","cg06116095","cg06628000","cg26963090","cg18988685","cg02788266","cg03068039","cg25890092","cg00939931","cg27027230"};
		
		String [][] b = {b1, b2, b3,b4, b5, b6, b7,b8,b9,b10,b11,b12,b13};
		int startId = 2000000001;
		checkForDuplicates(b);
		generateCases(b,cpgs,startId,outputFileName);
	}
	
	private void checkForDuplicates(String [][] b) {
		int index = 1;
		for (String [] l: b) {
			int inner = 1;
			for (String [] i: b) {
				if (index != inner) {
					boolean flag = isSame(i,l);
					if (flag) {
						System.out.println("Found same: " + index + " -> " + inner);
					}
				}
				inner ++;
			}
			index ++;
		}
	}
	
	public boolean isSame(String [] a, String [] b) {
		Map<String,String> aMap = new HashMap<String, String>();
		for (String s: a) {
			aMap.put(s, s);
		}
		for (String s: b) {
			if (!aMap.containsKey(s)) {
				return false;
			}
		}
		return true;
	}
	
	private void generateCases(String [][] b,String[] cpgs,int startId,String outputFileName)
		throws Exception {
		BufferedWriter bw = FileUtil.getBufferedWriter(outputFileName);
		int id = startId;
		for (String [] terms: b) {
			for (String cpg: cpgs) {
				boolean flag = contains(terms, cpg);
				if (!flag) {
					StringBuilder sb = new StringBuilder(id + "\t");
					for (String term: terms) {
						sb.append(term + "\t");
					}
					sb.append(cpg + "\n");
					bw.write(sb.toString());
					id ++;
				}
			}
		}
		System.out.println("Created " + (id-startId)+ " cases");
		
		bw.close();
	}
	
	private boolean contains(String [] b, String s) {
		for (String term: b) {
			if (term.equals(s)) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) throws Exception {
		MultiFeatureCasesGenerator vg = new MultiFeatureCasesGenerator();
		String outputFileName = GSE59999_Verify_CPG_Cases_Gen;
		String[] cpgs = { "cg06410630", "cg06669701", "cg06628000", "cg10461264", "cg18988685", "cg24616138",
				"cg27027230", "cg00936790", "cg14414100", "cg00939931", "cg06116095", "cg02788266", "cg03068039",
				"cg25890092", "cg19287711", "cg07033513", "cg07060505", "cg26963090" };
		//vg.generateThreeCases(cpgs,outputFileName);
		//vg.generateFourCases(cpgs,outputFileName);
		//vg.generateFiveCases(cpgs,outputFileName);
		//vg.generateSixCases(cpgs, outputFileName);
		//vg.generateSevenCases(cpgs, outputFileName);
		//vg.generateEightCases(cpgs, outputFileName);
		//vg.generateNineCases(cpgs, outputFileName);
		//vg.generateTenCases(cpgs, outputFileName);
		//vg.generateElevenCases(cpgs, outputFileName);
		vg.generateTwelveCases(cpgs, outputFileName);
	}
}