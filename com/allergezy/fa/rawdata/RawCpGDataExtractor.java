package com.allergezy.fa.rawdata;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * This file pulls data from the different cases and creates a file with values
 * for the attributes across all the examples. This is used for plotting the
 * graphs
 * 
 * @author Ayush Alag
 *
 */
public class RawCpGDataExtractor implements FoodAllergyConstants {

	/**
	 * Given a list of CPGs extract the raw values across the 58 cases
	 * 
	 * @param cpgs
	 *            -- list of specified CPGs
	 * @param fileName
	 *            -- file to write the output
	 * @throws Exception
	 */
	public void extractRelevantData(String[] cpgs, String fileName) throws Exception {
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		// Allergy and sensitized rows
		String[] allergyRows = FileUtil.readAllergyRowsFromFile();
		String[] senRows = FileUtil.readSenRowsFromFile();
		String[] nonAllergyRows = FileUtil.readNonAllergicRowsFromFile();
		//Index for all three datasets is the same, so we just need to generate it once
		Map<String, Integer> map = getIndexMap(cpgs,allergyRows);

		writeRowsToFile(0, Classification.Allergy.getValue(), cpgs, map, allergyRows, bw);
		writeRowsToFile(1, Classification.Sensitized.getValue(),  cpgs, map, senRows, bw);
		writeRowsToFile(1, -1, cpgs, map, nonAllergyRows, bw);

		bw.close();
		System.out.println("Finished writing to file .. " + fileName);
	}
	
	/**
	 * Helper method to generate a map containing the index for a cpg
	 * @param cpgs
	 * @param allergyRows
	 * @return
	 */
	private Map<String, Integer> getIndexMap(String[] cpgs, String[] allergyRows) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		for (String cpg : cpgs) {
			map.put(cpg, 0);
		}
		String[] terms = allergyRows[0].split("\t");
		int index = 0;
		for (String term : terms) {
			if (map.containsKey(term)) {
				map.put(term, index);
				System.out.println(term + " " + index);
			}
			index++;
		}
		return map;
	}

	/**
	 * Helper method to write the appropriate CpG methylation values to a file
	 * 
	 * @param startIndex
	 *            start row
	 * @param cpgs
	 *            list of CpGs
	 * @param map
	 *            map containing indexes for CpG
	 * @param rows
	 *            rows of data
	 * @param bw
	 *            buffered writer
	 * @throws IOException
	 */
	private void writeRowsToFile(int startIndex, int classificationValue, String[] cpgs, Map<String, Integer> map, String[] rows,
			BufferedWriter bw) throws IOException {
		for (int i = startIndex; i < rows.length; i++) {
			
			String[] cpgValues = rows[i].split("\t");
			StringBuilder sb = new StringBuilder(cpgValues[0] + "\t"+ classificationValue + "\t");
			for (String cpg : cpgs) {
				Integer value = map.get(cpg);
				String cpgValue = cpgValues[value];
				sb.append(cpgValue + "\t");
			}

			sb.append("\n");
			bw.write(sb.toString());
			bw.flush();
		}
	}

	// Main method to extract data
	public static void main(String[] args) throws Exception {
		String[] cpgs = { "cg06410630","cg06628000", "cg03068039","cg10461264","cg18988685","cg02788266","cg26963090",
				"cg19287711","cg00939931","cg25890092","cg07060505","cg06116095","cg24616138","cg14414100",
				"cg27027230","cg00936790","cg06669701","cg07033513"	
				 };
		//Duplicate "cg18988685",
		// String[] cpgs = { "cg00936790","cg07033513"};
		String fileName = GSE59999_Raw_CPG_Data;
		RawCpGDataExtractor v = new RawCpGDataExtractor();
		v.extractRelevantData(cpgs, fileName);
	}

}
