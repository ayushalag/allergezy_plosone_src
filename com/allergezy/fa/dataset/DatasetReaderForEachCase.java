package com.allergezy.fa.dataset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.kfold.KFoldDatasetInfo;
import com.allergezy.fa.util.FoodAllergyEnums.DataType;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * This Singleton class provides APIs for accessing the test, train, and hidden
 * datasets for each of the cases.
 * 
 * hello test
 * 
 * @author Ayush Alag
 *
 */
public class DatasetReaderForEachCase {
	private static DatasetReaderForEachCase instance = null;
	// Case number, rows of data for all examples
	private Map<Integer, String[]> caseDataMap = null;
	//Singleton that provides meta information about which examples are in which case
	private KFoldDatasetInfo kFoldDatasetInfo = null;

	/**
	 * Private constructor for Singleton
	 */
	private DatasetReaderForEachCase() {
		initialize();
	}

	/**
	 * Initialize the class variables
	 */
	private void initialize() {
		this.caseDataMap = new HashMap<Integer, String[]>();
		this.kFoldDatasetInfo = KFoldDatasetInfo.getInstance();
	}

	/**
	 * Get instance to Singleton
	 * 
	 * @return
	 */
	public static DatasetReaderForEachCase getInstance() {
		if (instance == null) {
			synchronized (DatasetReaderForEachCase.class) {
				instance = new DatasetReaderForEachCase();
			}
		}
		return instance;
	}
	

	/**
	 * This method provides the dataset for the specified case and DataType The
	 * first row contains column headers The first column contains the example
	 * name The last column contains the predicted class, as specified by
	 * Classification
	 * 
	 * @param dataType
	 *            the type of data
	 * @param caseNumber
	 *            case number
	 * @return
	 */
	public List<List<String>> getExamples(DataType dataType, int caseNumber) {
		List<List<String>> retDataset = new ArrayList<List<String>>();
		String [] rows = readDataFromFile(caseNumber);
		//Add the headers
		List<String> headers = new ArrayList<String>(rows[0].length());
		retDataset.add(headers);
		String [] terms = rows[0].split("\t");
		for (String header: terms) {
			headers.add(header);
		}
		//Now get the list of examples that need to be added
		List<String> examples = this.kFoldDatasetInfo.getExampleIds(dataType, caseNumber);
		Map<String,String> examplesMap = new HashMap<String,String>();
		for (String example: examples) {
			examplesMap.put(example, example);
		}
		//Iterate over all rows of data and check if its a valid example. Add to list if it is
		for (String row: rows) {
			terms = row.split("\t");
			if (examplesMap.containsKey(terms[0])) {
				List<String> exampleRow = new ArrayList<String>();
				retDataset.add(exampleRow);
				for (String term: terms) {
					exampleRow.add(term);
				}
			}
		}
		return retDataset;
	}

	/**
	 * Lazily read data for the caseNumber. Cache the data from file for efficiency
	 * @param caseNumber
	 * @return
	 */
	private String[] readDataFromFile(int caseNumber) {
		String[] retValue = this.caseDataMap.get(caseNumber);
		if (retValue == null) {
			String fileName = DatasetCreatorForEachCase.getDatasetFileName(caseNumber);
			retValue = InMemoryFileReader.getFileContentByRows(fileName);
			this.caseDataMap.put(caseNumber, retValue);
		}
		return retValue;
	}
	
	public static final void main(String [] args) {
		DatasetReaderForEachCase dr = DatasetReaderForEachCase.getInstance();
		for (int i = 0; i < 8; i ++) {
			for (DataType dataType: DataType.values()) {
				List<List<String>> data = dr.getExamples(dataType, i);
				System.out.println("Case " + i + " ... " + dataType);
				for (List<String> row: data) {
					for (String e: row) {
						System.out.print(e + ",");
					}
					System.out.println("");
				}
			}
		}
	}
}
