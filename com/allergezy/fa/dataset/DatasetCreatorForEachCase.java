package com.allergezy.fa.dataset;

import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.geo2r.Geo2rCpgInfo;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * This class generates the machine learning dataset for each of the eight
 * cases/epochs. Columns are cpg values, ordered by the importance of each cpg.
 * Rows are each examples First row contains the column headers with first being
 * the name of the example, followed by cpg name, last is the classification
 */
public class DatasetCreatorForEachCase implements FoodAllergyConstants {

	/**
	 * Creates datasets for each of the cases
	 * 
	 * @param numCpgs
	 * @throws Exception
	 */
	public void createDatasetsForEachCase(int numCpgs) throws Exception {
		String[] allergyRows = FileUtil.readAllergyRowsFromFile();
		String[] senRows = FileUtil.readSenRowsFromFile();

		for (int i = 0; i < 8; i++) {
			String fileName = getDatasetFileName(i);
			List<String> cpgNames = getOrderedCPGs(i);
			BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
			writeToFile(bw, allergyRows, cpgNames, numCpgs, Classification.Allergy, true);
			writeToFile(bw, senRows, cpgNames, numCpgs, Classification.Sensitized, false);
			bw.close();
		}
	}

	/**
	 * Create the non-allergic 13 cases dataset
	 * 
	 * @param epochNumber
	 * @return
	 */
	public void createNonAllergicDatasetsForEachCase(int numCpgs) throws Exception {
		String[] nonAllergicRows = FileUtil.readNonAllergicRowsFromFile();
		List<String> cpgNames = getOrderedCPGs(0);
		BufferedWriter bw = FileUtil.getBufferedWriter(GSE59999_40_10_8_NonAllergic_CPG_Data);
		writeToFile(bw, nonAllergicRows, cpgNames, numCpgs, Classification.Sensitized, true);
		bw.close();
	}

	/**
	 * Get the geo2r ordered list of CpG for the specified case
	 * @param epochNumber
	 * @return
	 */
	protected List<String> getOrderedCPGs(int epochNumber) {
		return Geo2rCpgInfo.getInstance().getOrderedCPGs(epochNumber);
	}

	/**
	 * Get the file name to create the dataset for the case
	 * @param caseNumber
	 * @return
	 */
	public static String getDatasetFileName(int caseNumber) {
		return GSE59999_40_10_8_CPG_Data + caseNumber + ".txt";
	}

	/**
	 * Helper method to write the ordered list of examples
	 * 
	 * @param bw             Bufferedwrite to file
	 * @param rows           Rows of example data that need to be pruned
	 * @param cpgNames       Ordered list of cpgs
	 * @param numCpgs        Number of cpg columns
	 * @param classification Classification
	 * @throws Exception
	 */
	private void writeToFile(BufferedWriter bw, String[] rows, List<String> cpgNames, int numCpgs,
			Classification classification, boolean addHeader) throws Exception {
		// Map the cpgs to their position in the reference dataset
		Map<String, Integer> map = new HashMap<String, Integer>();
		String[] cpgs = rows[0].split("\t");
		int index = 0;
		for (String cpg : cpgs) {
			map.put(cpg, index);
			index++;
		}
		// Write to file iterating over all examples
		StringBuilder sb = new StringBuilder();
		int startIndex = 1;
		if (addHeader) {
			startIndex = 0;
		}
		for (int j = startIndex; j < rows.length; j++) {
			String[] terms = rows[j].split("\t");
			sb.append(terms[0] + "\t");
			for (int k = 0; k < numCpgs; k++) {
				String cpg = cpgNames.get(k);
				sb.append(terms[map.get(cpg)] + "\t");
			}
			// Write the final classification
			if (j > 0) {
				sb.append(classification.getValue());
			} else {
				sb.append("Classification");
			}
			sb.append("\n");
		}
		bw.write(sb.toString());
		bw.flush();
	}

	public static void main(String[] args) throws Exception {
		int numCpgs = 99;
		DatasetCreatorForEachCase dc = new DatasetCreatorForEachCase();
	    dc.createDatasetsForEachCase(numCpgs);
		dc.createNonAllergicDatasetsForEachCase(numCpgs);
	}

}
