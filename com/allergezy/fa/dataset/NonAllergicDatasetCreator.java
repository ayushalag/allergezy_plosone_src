package com.allergezy.fa.dataset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Singleton class to return values for non-allergic samples
 * 
 * @author Ayush Alag
 *
 */
public class NonAllergicDatasetCreator implements FoodAllergyConstants {
	private static NonAllergicDatasetCreator instance = null;
	private Map<String,Integer> cpgToColIdMap = null;
	private String [] rows = null;

	private NonAllergicDatasetCreator() {
		initialize();
	}

	// Get Instance
	public static NonAllergicDatasetCreator getInstance() {
		if (instance == null) {
			synchronized (NonAllergicDatasetCreator.class) {
				instance = new NonAllergicDatasetCreator();
			}
		}
		return instance;
	}
	
	private void initialize() {
		this.rows = InMemoryFileReader.getFileContentByRows(GSE59999NonAllergicFile);
		this.cpgToColIdMap = new HashMap<String,Integer>();
		String [] terms = this.rows[0].split("\t"); //CPG headers
		for (int i = 0; i < terms.length; i ++) {
			String cpg = terms[i];
			this.cpgToColIdMap.put(cpg, i);
		}
	}
	
	public Integer getCpgIndexId(String cpg) {
		return this.cpgToColIdMap.get(cpg);
	}
	
	public List<List<String>> getDataValuesForCPG(String [] cpgs) {
		List<List<String>> dataValues = new ArrayList<List<String>>();
		List<String> values = new ArrayList<String>();
		for (String cpg: cpgs) {
			values.add(cpg);
		}
		values.add("Prediction");
		dataValues.add(values);
		for (int i = 1; i < this.rows.length; i ++) {
			String [] terms = this.rows[i].split("\t");
			values = new ArrayList<String>();
			dataValues.add(values);
			values.add(terms[0]);
			
			for (String cpg: cpgs) {
			//	System.out.println(cpg);
				int index = this.cpgToColIdMap.get(cpg);
				values.add(terms[index]);
			}
			//Add a 0 for the last column -- non Allergic
			values.add("0");
		}
		return dataValues;
	}
	
	public static void main(String [] args) {
		NonAllergicDatasetCreator dc = NonAllergicDatasetCreator.getInstance();
		String [] cpgs = {"cg06410630","cg06628000","cg03068039","cg10461264","cg18988685",
				"cg02788266","cg26963090","cg19287711","cg00939931","cg25890092","cg07060505",
				"cg06116095","cg24616138","cg14414100","cg18988685","cg27027230","cg00936790",
				"cg06669701"
		};
		for (String cpg: cpgs) {
			System.out.println(cpg + " ->" + dc.getCpgIndexId(cpg));
		}
		List<List<String>> dataValues = dc.getDataValuesForCPG(cpgs);
		for (List<String> row: dataValues) {
			for(String s: row) {
				System.out.print(s +",");
			}
			System.out.println("");
		}
	}

}
