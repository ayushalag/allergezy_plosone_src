package com.allergezy.fa.featureanalyzer;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.geo2r.CPGStatistics;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Analyzes the results specified and gets the frequency of occurence for the different CPGs
 * 1 cg16547579 [2481-3,4,5,6,7,8,9,10,11,12,13,16,17,18,20,21,23,25,26,27,30,31,32,35,38,41,42,44,45,46,48,49,57,] -- {SLC23A2;SLC23A2}
 * 2 cg10336671 [2064-6,7,10,12,13,16,18,19,20,21,22,24,25,26,30,31,33,35,36,37,38,41,42,43,44,45,46,47,49,] -- {GHRHR;GHRHR}
 * 3 cg18798744 [1568-4,5,6,9,10,12,16,19,20,21,24,25,29,32,37,38,41,43,46,47,48,] -- {DNAJC18}
 * @author Ayush Alag
 *
 */
public class CpGFrequencyFeatureAnalyzer implements FoodAllergyConstants {
	/**
	 * Analyzes the frequency of different features
	 * 
	 * @param inputFileName
	 * @param outputFileName
	 */
	public void analyzeFeatures(String inputFileName, String outputFileName) throws Exception {
		String[] rows = InMemoryFileReader.getFileContentByRows(inputFileName);
		Map<String, CPGStatistics> cpgMap = new HashMap<String, CPGStatistics>();
		Map<CPGStatistics, Integer> cpgStats = new HashMap<CPGStatistics, Integer>();
		int position = 0;
		for (String row : rows) {
			position++;
			String[] terms = row.split("\t");
			for (String term : terms) {
				if (term.startsWith("cg")) {
					String cpg = term;				
					CPGStatistics stats = cpgMap.get(cpg);
					if (stats == null) {
						stats = new CPGStatistics(cpg);
						cpgMap.put(cpg, stats);
					}
					Integer count = cpgStats.get(stats);
					if (count == null) {
						count = new Integer(0);
					}
					count++;
					stats.addPosition(position);
					cpgStats.put(stats, count);
				}
			}
		}
		List<CPGStatistics> statList = new ArrayList<CPGStatistics>();
		for (CPGStatistics stat : cpgStats.keySet()) {
			statList.add(stat);
		}

		Collections.sort(statList);
		BufferedWriter bw = FileUtil.getBufferedWriter(outputFileName);
		int rank = 1;
		for (CPGStatistics stat : statList) {
			bw.write(rank + " " + stat + "\n");
			rank ++;
		}
		bw.flush();
		bw.close();
	}

	public static void main(String[] args) throws Exception {
		String inputFileName = GSE59999_Verify_Cases_InputFileName;
		String outputFileName = GSE59999_Verify_FeatureAnalyzer_CaseResult;
		CpGFrequencyFeatureAnalyzer va = new CpGFrequencyFeatureAnalyzer();
		va.analyzeFeatures(inputFileName, outputFileName);
	}
}
