package com.allergezy.fa.featureanalyzer;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.geo2r.CPGAnnotations;
import com.allergezy.fa.multifeature.reader.VerifyCasesReader;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * This class looks at the cases in the verify/setup/summary.txt file and annotates the genes base
 * based on the frequency count of the CPGs
 * 1	cg16547579	SLC23A2;SLC23A2	18
 * 2	cg10336671	GHRHR;GHRHR	15
 * 3	cg18798744	DNAJC18	12
 * @author Ayush Alag
 *
 */
public class CpGGeneAnnotator implements FoodAllergyConstants {

	/**
	 * Annotate the CPGs for their genes and write to the specified file
	 * 
	 * @param fileName
	 * @throws Exception
	 */
	public void annotateGenes(String fileName) throws Exception {
		VerifyCasesReader cv = VerifyCasesReader.getInstance();
		List<List<String>> egs = cv.getExamples();
		Map<String, Integer> cpgCounts = new HashMap<String, Integer>();
		for (List<String> eg : egs) {
			for (String t : eg) {
				if (t.startsWith("cg")) {
					Integer c = cpgCounts.get(t);
					if (c == null) {
						c = 0;
					}
					c++;
					cpgCounts.put(t, c);
				}
			}
		}
		List<CPGCount> cpgs = new ArrayList<CPGCount>();
		for (String s : cpgCounts.keySet()) {
			CPGCount cpg = new CPGCount(s, cpgCounts.get(s));
			cpgs.add(cpg);
		}
		Collections.sort(cpgs);
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		int index = 1;
		for (CPGCount cpg : cpgs) {
			bw.write("" + index + "\t" + cpg.getCPG() + "\t" + cpg.getGene() + "\t" + cpg.getCount() + "\n");
			index ++;
		}
		bw.flush();
		bw.close();
	}

	/**
	 * Inner class to sort the CpGs
	 * @author Ayush Alag
	 *
	 */
	private class CPGCount implements Comparable<CPGCount> {
		int count;
		String cpg;

		public CPGCount(String cpg, int count) {
			this.count = count;
			this.cpg = cpg;
		}

		public String getCPG() {
			return this.cpg;
		}

		public int getCount() {
			return this.count;
		}

		public String getGene() {
			CPGAnnotations a = CPGAnnotations.getInstance();
			String gene = a.getGeneMapping(cpg);
			return gene;
		}

		@Override
		public int compareTo(CPGCount o) {
			return o.count - count;
		}

	}

	/**
	 * Run the annotations
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		CpGGeneAnnotator a = new CpGGeneAnnotator();
		String fileName = GSE59999_CPG_GeneAnnotation;
		a.annotateGenes(fileName);
	}

}
