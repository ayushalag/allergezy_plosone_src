package com.allergezy.fa.weka;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import com.allergezy.fa.dataset.DatasetReaderForEachCase;
import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FoodAllergyConstants;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This is the main class for interfacing with Weka APIs Weka consists of a
 * Dataset/Instances that contains a list of Attributes and Instaces. Each
 * Instance is an example.
 * 
 * This is also implemented as a Singleton class
 * 
 * @author Ayush Alag
 *
 */
public class WekaDatasetBuilder implements FoodAllergyConstants {
	private static WekaDatasetBuilder instance = null;
	private DatasetReaderForEachCase datasetReaderForEachCase = null;

	/**
	 * Private constructor
	 * 
	 */
	protected WekaDatasetBuilder() {
		this.datasetReaderForEachCase = DatasetReaderForEachCase.getInstance();
	}

	/**
	 * Get instance for the Singleton
	 * 
	 * @return
	 */
	public static WekaDatasetBuilder getInstance() {
		if (instance == null) {
			synchronized (WekaDatasetBuilder.class) {
				instance = new WekaDatasetBuilder();
			}
		}
		return instance;
	}
	
	protected List<List<String>> getDataValues(DataType dataType, int caseNumber,int numDimensions) {
		 return this.datasetReaderForEachCase.getExamples(dataType, caseNumber);
	}

	/**
	 * This class executes the training of a classifier, specified by the
	 * Algorithm, and the number of dimension or CPGs to be used. After learning
	 * the model, it evaluates the models against training, test, and hidden
	 * datasets. Along the way it will call the list of writers that write
	 * results to appropriate files.
	 * 
	 * @param caseId
	 *            to be used as input
	 * @param epochId
	 *            the epoch or case number
	 * @param algorithm
	 *            Algorithm to be used
	 * @param resultWriters
	 *            writes results to appropriate files
	 * @throws Exception
	 */
	public void executeLearningAnalysis(int caseId, int epochId, Algorithm algorithm,
			List<ResultWriter> resultWriters, WekaHelper wekaHelper) throws Exception {
		// Get the training, test, and hidden datasets
		List<List<String>> dataValues =  getDataValues(DataType.TRAIN,  epochId, caseId) ;	
		//printValues("Dataset " + caseId + " " + epochId, dataValues);
		ArrayList<Attribute> attributes = wekaHelper.createAttributes(dataValues.get(0), 
				caseId);
		//Instances for training, test, hidden
		Instances trainingInstances = wekaHelper.createDataset(DataType.TRAIN.getName(), 
				dataValues, attributes,caseId);
		dataValues = getDataValues(DataType.TEST,  epochId, caseId) ;
		Instances testInstances = wekaHelper.createDataset(DataType.TEST.getName(), 
				dataValues, attributes,caseId);
		dataValues = getDataValues(DataType.HIDDEN,  epochId, caseId) ;
		Instances hiddenInstances = wekaHelper.createDataset(DataType.HIDDEN.getName(), 
				dataValues, attributes,caseId);

		//Get the classifier and build it using the training examples
		Classifier classifier = wekaHelper.getClassifier(algorithm);
		//Train the classifier with the training data
		classifier.buildClassifier(trainingInstances);
		if (classifier instanceof J48) {
			System.out.println(((J48) classifier).graph());
		}
		//Evaluate the classifier
		Evaluation trainingModelEvaluation = new Evaluation(trainingInstances);
		trainingModelEvaluation.evaluateModel(classifier, trainingInstances);
		Evaluation testModelEvaluation = new Evaluation(testInstances);
		testModelEvaluation.evaluateModel(classifier, testInstances);
		Evaluation hiddenModelEvaluation = new Evaluation(hiddenInstances);
		hiddenModelEvaluation.evaluateModel(classifier, hiddenInstances);
	
		//Call the visitors to print out the results
		EpochResultHolder epochResultHolder = new EpochResultHolder(caseId, epochId, algorithm, attributes, classifier,trainingInstances,
				testInstances,hiddenInstances,trainingModelEvaluation,testModelEvaluation,
				hiddenModelEvaluation);
		for (ResultWriter resultWriter: resultWriters) {	
			resultWriter.visit(epochResultHolder);
		}
	}
	
	private void printValues(String text, List<List<String>> dataValues) {
		System.out.println("\n" + text);
		for (List<String> row:dataValues) {
			System.out.println();
			for (String value: row) {
				System.out.print(value + ",");
			}
		}
	}
	
	public static void main(String [] args) throws Exception {
		WekaDatasetBuilder wb = WekaDatasetBuilder.getInstance();
		DatasetReaderForEachCase dr = DatasetReaderForEachCase.getInstance();
		List<List<String>> dataValues = dr.getExamples(DataType.TRAIN, 0);
		int numDimensions = 2;
		WekaHelper wh = new WekaHelper();
		ArrayList<Attribute> attributes = wh.createAttributes(dataValues.get(0), numDimensions);
		for (Attribute attribute: attributes) {
			System.out.println(attribute);
		}
		
		WekaHelper wekaHelper = new WekaHelper();
		
		Instances dataset = wekaHelper.createDataset(DataType.TRAIN.getName(), 
				dataValues, attributes,numDimensions);
		Classifier classifier = wekaHelper.getClassifier(Algorithm.MLP);
		classifier.buildClassifier(dataset);
		Evaluation modelEvaluation = new Evaluation(dataset);
		modelEvaluation.evaluateModel(classifier, dataset);
		
		StringBuilder sb = new StringBuilder();
		for (Enumeration<Instance> e = dataset.enumerateInstances(); e.hasMoreElements();) {
			Instance instance = e.nextElement();
			double prediction = classifier.classifyInstance(instance);
			sb.append("" + prediction);
			double[] distribution = classifier.distributionForInstance(instance);
			sb.append("\t" + "[" + distribution[0] + "]\n");
		}
		System.out.println(sb.toString());
		ArrayList<Prediction> predictions = modelEvaluation.predictions();
		for (Prediction prediction: predictions) {
			sb.append("Predictions: " + prediction + "\n");
		}
	}

}
