package com.allergezy.fa.weka;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.util.FoodAllergyConstants;

import weka.classifiers.Classifier;
import weka.classifiers.functions.LibSVM;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.functions.RBFClassifier;
import weka.classifiers.functions.SimpleLogistic;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Singleton class that provide useful methods to interface with Weka.
 * This is a helper class to convert data into WEKA objects using Weka APIs
 * 
 * @author Ayush Alag
 *
 */
public class WekaHelper implements FoodAllergyConstants {

	/**
	 * Converts the specified Algorithm to a Weka Classifier
	 * 
	 * @param algorithm
	 * @return
	 */
	public Classifier getClassifier(Algorithm algorithm) {
		Classifier classifier = null;
		if (Algorithm.RBF.equals(algorithm)) {
			// http://www.cs.waikato.ac.nz/ml/publications/2014/rbf_networks_in_weka_description.pdf
			RBFClassifier rbfLearner = new RBFClassifier();
			// rbfLearner.setNumClusters(10);
			classifier = rbfLearner;
		}
		// MultilayerPerceptron
		if (Algorithm.MLP.equals(algorithm)) {
			// http://www.cs.waikato.ac.nz/ml/publications/2014/rbf_networks_in_weka_description.pdf
			MultilayerPerceptron mlp = new MultilayerPerceptron();
			mlp.setLearningRate(0.1);
			mlp.setMomentum(0.2);
			mlp.setTrainingTime(20000);
			mlp.setHiddenLayers("10,10"); // 10,10
			// mlp.setHiddenLayers("10"); //10,10
			classifier = mlp;
		}
		if (Algorithm.LOGISTIC_REGRESSION.equals(algorithm)) {
			SimpleLogistic sl = new SimpleLogistic();
			classifier = sl;
		}
		if (Algorithm.DECISION_TREE.equals(algorithm)) {
			J48 decisionTree = new J48();
			classifier = decisionTree;
		}
		if (Algorithm.SVM.equals(algorithm)) {
			LibSVM svm = new LibSVM();
			classifier = svm;
		}
		return classifier;
	}

	/**
	 * Create a list of attributes. It ignores the first column, since its the
	 * name of the sample. Assumes that the last column is the predicted
	 * classification. This is a nominal attribute with two values, while the
	 * rest are continuous attributes
	 * 
	 * @param headers
	 *            the headers for the dataset
	 * @return
	 */
	public ArrayList<Attribute> createAttributes(List<String> headers, int caseId) {
		// Col 1 is name of sample - ignore
		// The last column is predicted, rest are attributes of interest
		ArrayList<Attribute> allAttributes = getFeatures(headers, caseId);

		// The predicted attribute is nominal
		// Create list to hold nominal values "first", "second", "third"
		List<String> values = new ArrayList<String>(2);
		values.add("" + Classification.Allergy.getValue());
		values.add("" + Classification.Sensitized.getValue());
		Attribute resultAttribute = new Attribute(Classification.Allergy.getName(), values);
		allAttributes.add(resultAttribute);

		return allAttributes;
	}

	/**
	 * Get the features that will be used to learn
	 * @param headers
	 * @param caseId
	 * @return
	 */
	protected ArrayList<Attribute> getFeatures(List<String> headers, int caseId) {
		// Col 1 is name of sample - ignore
		// The last column is predicted, rest are attributes of interest
		ArrayList<Attribute> allAttributes = new ArrayList<Attribute>(caseId + 1);
		// We can ignore the last class
		int index = 0;
		for (String header : headers) {
			addFeature( index,  caseId,  headers,
					allAttributes,  header);
			index++;
		}
		return allAttributes;
	}
	
	protected void addFeature(int index, int numDimensions, List<String> headers,
			ArrayList<Attribute> allAttributes, String header) {
		if ((index > 0) && (index <= numDimensions)) {
			Attribute attribue = new Attribute(header); // continuous
														// attribute
			allAttributes.add(attribue);
		}
	}

	/**
	 * An instance is one of the examples. The first coulmn is ignored, since it
	 * is the name of the sample. The last column is the predicted attribute.
	 * 
	 * This method converts one example into an Instance
	 * 
	 * The newly populated Instance is added to the instances object
	 * 
	 * @param terms
	 *            the terms for this example
	 * @param attributes
	 *            the master list of attributes
	 * @param instances
	 *            the dataset that contains all the instances
	 * @return
	 */
	public Instance createInstance(List<String> terms, ArrayList<Attribute> attributes, Instances instances,
			int numFeatures) {
		Instance instance = new DenseInstance(attributes.size());
		instance.setDataset(instances);
		int count = 0;
		boolean isFirst = true;
		for (String term : terms) {
			if (isFirst) {
				isFirst = false;
			} else {
				addFeatureValue(count,  numFeatures, term, instance);
				count++;
			}
		}
		
		// Now populate the classification to be learnt
		instance.setValue(attributes.size() - 1, terms.get(terms.size() - 1));
		return instance;
	}
	
	protected void addFeatureValue(int count, int numFeatures, String term, Instance instance) {
		if (count <= numFeatures) {
			double d = Double.parseDouble(term);
			instance.setValue(count, d);
		}
	}

	/**
	 * Creates a dataset, i.e., instances, consisting of examples of instances
	 * 
	 * The first row is ignored, as it contains the header
	 * 
	 * @param name
	 *            name for the dataset
	 * @param rows
	 *            data to be converted to Instances
	 * @param attributes
	 *            the list of attributes
	 * @return
	 */
	public Instances createDataset(String name, List<List<String>> rows, ArrayList<Attribute> attributes,
			int numFeatures) {
		Instances instances = new Instances(name, attributes, rows.size() - 1);
		instances.setClassIndex(attributes.size() - 1); // index starts with 0
		boolean isFirst = true;
		for (List<String> row : rows) {
			if (isFirst) {
				isFirst = false;
			} else {
				Instance instance = createInstance(row, attributes, instances, numFeatures);
				instances.add(instance);
			}
		}
		return instances;
	}
}
