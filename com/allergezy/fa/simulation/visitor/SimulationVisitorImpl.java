package com.allergezy.fa.simulation.visitor;

import java.io.BufferedWriter;

import com.allergezy.fa.simulation.dataset.SimulationDataset;
import com.allergezy.fa.simulation.dataset.SimulationDatasetEvaluation;
import com.allergezy.fa.util.FileUtil;

public class SimulationVisitorImpl implements SimulationVisitor {
	private BufferedWriter bw = null;

	public SimulationVisitorImpl(String fileName) {
		this.bw = FileUtil.getBufferedWriter(fileName);
		this.initialize();
	}

	protected void initialize() {
		StringBuilder sb = new StringBuilder();
		sb.append("Algorithm" + "\t" + "Training" + "\t" + "Training AUROC" + "\t");
		sb.append("Test" + "\t" + "Test ROC" + "\t");
		sb.append("Hidden" + "\t" + "Hidden ROC");
		sb.append("\n");
	}

	public void setup(int modelId) {

	}

	public void visit(SimulationDataset dataset, SimulationDatasetEvaluation e) {
		StringBuilder sb = new StringBuilder();
		sb.append(e.getAlgorithm() + "\t" + e.getTrainingModelEvaluation().pctCorrect() + "\t"
				+ e.getTrainingModelEvaluation().areaUnderROC(0) + "\t");
		sb.append(e.getTestModelEvaluation().pctCorrect() + "\t" + e.getTestModelEvaluation().areaUnderROC(0) + "\t");
		sb.append(e.getHiddenModelEvaluation().pctCorrect() + "\t" + e.getHiddenModelEvaluation().areaUnderROC(0));
		sb.append("\n");
		try {
			bw.write(sb.toString());
			bw.flush();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	protected void write(String str) throws Exception {
		bw.write(str);
		bw.flush();
	}

	public void cleanup(double per) throws Exception {
		bw.write(per + "\n");
		bw.flush();
		bw.close();
	}

	public void allDone() throws Exception {
		bw.flush();
		bw.close();
	}
}
