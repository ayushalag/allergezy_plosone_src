package com.allergezy.fa.simulation.visitor;

import com.allergezy.fa.simulation.dataset.SimulationDataset;
import com.allergezy.fa.simulation.dataset.SimulationDatasetEvaluation;
import com.allergezy.fa.util.FoodAllergyConstants;

public interface SimulationVisitor extends FoodAllergyConstants {
	public void setup(int modelId);
	public void visit(SimulationDataset dataset, SimulationDatasetEvaluation e);
	public void cleanup(double per) throws Exception;
	public void allDone() throws Exception;
}
