package com.allergezy.fa.simulation.visitor;

import com.allergezy.fa.simulation.dataset.SimulationDataset;
import com.allergezy.fa.simulation.dataset.SimulationDatasetEvaluation;

public class SummarySimulationVisitorImpl extends SimulationVisitorImpl {
	private int modelId;
	private double trainCorrect = 0.;
	private double trainROC = 0.;
	private double testCorrect = 0.;
	private double testROC = 0.;
	private double hiddenCorrect = 0.;
	private double hiddenROC = 0.;
	private int count;

	public SummarySimulationVisitorImpl(String fileName) {
		super(fileName);
		this.modelId = 0;
	}

	public void setup(int modelId) {
		this.modelId = modelId;
	}

	protected void initialize() {
		StringBuilder sb = new StringBuilder();
		sb.append("ModelId" + "\t" + "Count" + "\t" +  "Training" + "\t" + "Training AUROC" + "\t");
		sb.append("Test" + "\t" + "Test ROC" + "\t");
		sb.append("Hidden" + "\t" + "Hidden ROC");
		sb.append("\n");
	}

	public void visit(SimulationDataset dataset, SimulationDatasetEvaluation e) {
		this.trainCorrect += e.getTrainingModelEvaluation().pctCorrect();
		this.trainROC += e.getTrainingModelEvaluation().areaUnderROC(0);
		this.testCorrect += e.getTestModelEvaluation().pctCorrect();
		this.testROC += e.getTestModelEvaluation().areaUnderROC(0);
		this.hiddenCorrect += e.getHiddenModelEvaluation().pctCorrect();
		this.hiddenROC += e.getHiddenModelEvaluation().areaUnderROC(0);
		this.count++;
	}

	public void cleanup(double per) throws Exception {
		write(modelId + "\t" + this.count + "\t" +  this.trainCorrect / count + "\t" + this.trainROC / count + "\t");
		write(this.testCorrect / count + "\t" + this.testROC / count + "\t");
		write(this.hiddenCorrect / count + "\t" + this.hiddenROC / count + "\n");
		this.trainCorrect = 0.;
		this.trainROC = 0.;
		this.testCorrect = 0.;
		this.testROC = 0.;
		this.hiddenCorrect = 0.;
		this.hiddenROC = 0.;
		this.count = 0;
	}
	
	
}
