package com.allergezy.fa.simulation.dataset;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.allergezy.fa.kfold.KFoldDatasetGenerator;
import com.allergezy.fa.rawdata.RawCpGDataExtractor;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * In this class a dataset is created for a random simulation Reference:
 * com.allergezy.fa.kfold.KFoldDatasetGenerator
 * 
 * @author Ayush Alag
 *
 */
public class DatasetBuilder extends KFoldDatasetGenerator {
	private List<String> allergyIds = null;
	private List<String> senIds = null;
	private static DatasetBuilder instance = null;
	private static final String sampleIdsFileName = GSE59999_ALL_SAMPLE_IDs;
	private static final String cpgIdsFileName = GSE59999_CPG_IDs;
	private static final String datasetFileName = GSE59999_Dataset_File;
	private String[] cpgIds = null;
	private String[] datasetRows = null;
	private Map<String, String[]> sampleIdDataMap = null;
	private Map<String,Integer> cpgToIdMap = null;

	private DatasetBuilder() {
		initialize();
	}

	/**
	 * Get instance
	 * 
	 * @return
	 */
	public static DatasetBuilder getInstance() {
		if (instance == null) {
			synchronized (DatasetBuilder.class) {
				instance = new DatasetBuilder();
			}
		}
		return instance;
	}

	/**
	 * Read in the sample ids for allergy and sensitized samples
	 * 
	 */
	private void initialize() {
		this.allergyIds = new ArrayList<String>();
		this.senIds = new ArrayList<String>();
		String[] rows = InMemoryFileReader.getFileContentByRows(sampleIdsFileName);
		if ((rows == null) || (rows.length < 58)) {
			createSampleIdsFile();
			rows = InMemoryFileReader.getFileContentByRows(sampleIdsFileName);
		}
		for (int i = 0; i < rows.length; i++) {
			String[] terms = rows[i].split(" ");
			if (terms[2].compareTo("1") == 0) {
				allergyIds.add(terms[1]);
			} else {
				senIds.add(terms[1]);
			}
		}
		this.cpgIds = InMemoryFileReader.getFileContentByRows(cpgIdsFileName);
		cpgToIdMap = new HashMap<String,Integer>();
		int index = 0;
		for (String cpg: this.cpgIds) {
			this.cpgToIdMap.put(cpg, index ++);
		}
		this.readDatasetRows();
	}
	
	/**
	 * Get CPG index out of 18
	 * @param cpg
	 * @return
	 */
	public Integer getCpgIndex(String cpg) {
		return this.cpgToIdMap.get(cpg);
	}

	/**
	 * read the raw data
	 */
	private void readDatasetRows() {
		this.datasetRows = InMemoryFileReader.getFileContentByRows(datasetFileName);
		if ((datasetRows == null) || (datasetRows.length < 58)) {
			RawCpGDataExtractor e = new RawCpGDataExtractor();
			try {
				e.extractRelevantData(this.cpgIds, this.datasetFileName);
				this.datasetRows = InMemoryFileReader.getFileContentByRows(datasetFileName);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
		this.sampleIdDataMap = new HashMap<String, String[]>();
		for (int i = 0; i < this.datasetRows.length; i++) {
			String[] terms = this.datasetRows[i].split("\t");
			System.out.println(terms[0]);
			this.sampleIdDataMap.put(terms[0], terms);
		}
	}

	/**
	 * Shuffle the list baset on the seed
	 * @param seed
	 * @param orig
	 * @return
	 */
	public List<String> getShuffledList(int seed, List<String> orig) {
		List<String> ans = new ArrayList<String>(orig.size());
		ans.addAll(orig);
		Collections.shuffle(ans, new Random(seed));
		return ans;
	}
	
	/**
	 * Create a random simulation dataset using the provided seed
	 * @param seed
	 * @return
	 */
	public SimulationDataset createSimulationDataset(int seed) {
		SimulationDataset dataset = new SimulationDataset(this.cpgIds, this.sampleIdDataMap);
		List<String> sAllergy = getShuffledList(seed, this.allergyIds);
		List<String> sSen = getShuffledList(seed, this.senIds);
		
		List<String> allergyTrainIds = new ArrayList<String>();
		List<String> allergyTestIds = new ArrayList<String>();
		List<String> allergyHiddenIds = new ArrayList<String>();
		
		List<String> senTrainIds = new ArrayList<String>();
		List<String> senTestIds = new ArrayList<String>();
		List<String> senHiddenIds = new ArrayList<String>();
		for (int i = 0; i < 20; i ++) {
			allergyTrainIds.add(sAllergy.get(i));
			senTrainIds.add(sSen.get(i));
		}
		for (int i = 20; i < 25; i ++) {
			allergyTestIds.add(sAllergy.get(i));
			senTestIds.add(sSen.get(i));
		}
		for (int i = 25; i < 29; i ++) {
			allergyHiddenIds.add(sAllergy.get(i));
			senHiddenIds.add(sSen.get(i));
		}
		
		dataset.setAllergyHiddenIds(allergyHiddenIds);
		dataset.setAllergyTestIds(allergyTestIds);
		dataset.setAllergyTrainIds(allergyTrainIds);
		
		dataset.setSenTrainIds(senTrainIds);
		dataset.setSenTestIds(senTestIds);
		dataset.setSenHiddenIds(senHiddenIds);
		
		return dataset;
	}

	public String[] getDatasetRows() {
		return this.datasetRows;
	}

	public String[] getCpgIds() {
		return cpgIds;
	}

	public void setCpgIds(String[] cpgIds) {
		this.cpgIds = cpgIds;
	}

	public List<String> getAllergyIds() {
		return allergyIds;
	}

	public void setAllergyIds(List<String> allergyIds) {
		this.allergyIds = allergyIds;
	}

	public List<String> getSenIds() {
		return senIds;
	}

	public void setSenIds(List<String> senIds) {
		this.senIds = senIds;
	}

	public String[] getSampleValues(String sampleId) {
		return this.sampleIdDataMap.get(sampleId);
	}

	/**
	 * This method writes the names of the allergy and sensitized sample ids to
	 * a a file GSE59999_ALL_SAMPLE_IDs
	 * 
	 * @throws Exception
	 */
	public void createSampleIdsFile() {
		try {
			BufferedWriter bw = FileUtil.getBufferedWriter(sampleIdsFileName);
			List<String> allergyIds = getAllergyNames();
			int index = 1;
			for (String id : allergyIds) {
				bw.write(index++ + " " + id + " 1\n");
			}
			List<String> senIds = getSensitizedNames();
			index = 1;
			for (String id : senIds) {
				bw.write(index++ + " " + id + " 0\n");
			}
			bw.flush();
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		DatasetBuilder b = DatasetBuilder.getInstance();
		List<String> allergyIds = b.getAllergyIds();
		for (String id : allergyIds) {
			// System.out.println(id);
		}
		String[] cpgIds = b.getCpgIds();
		for (int i = 0; i < cpgIds.length; i++) {
			// System.out.println(i + 1 + " " + cpgIds[i]);
		}
		String[] rows = b.getDatasetRows();
		System.out.println(rows[0]);
		String sampleId = "GSM1463328";
		String[] values = b.getSampleValues(sampleId);
		System.out.println(values[2]);

		// Shuffle list
		List<String> allergyids = b.getAllergyIds();
		for (int i = 0; i < 10; i++) {
			List<String> s = b.getShuffledList(i + 1, allergyids);
			System.out.println(i + " " + s.get(0) + " " + s.get(1) + " " + s.get(2) + " " + s.get(3) + " " + s.get(4)
					+ " " + s.get(5) + " " + s.get(6)+ " " + s.get(7)+ " " + s.get(8)+ " " + s.get(9));
		}
		
		//Dataset
		SimulationDataset dataset = b.createSimulationDataset(1);
		System.out.println(dataset);
		System.out.println(b.createSimulationDataset(2));	
		System.out.println(b.createSimulationDataset(3));	
	}
}
