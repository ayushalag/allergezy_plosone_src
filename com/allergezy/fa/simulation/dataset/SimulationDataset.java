package com.allergezy.fa.simulation.dataset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.util.FoodAllergyConstants;

import weka.core.Instances;

/**
 * Dataset for a simulation
 * @author Ayush Alag
 *
 */
public class SimulationDataset implements FoodAllergyConstants {
	private List<String> allergyTrainIds = null;
	private List<String> allergyTestIds = null;
	private List<String> allergyHiddenIds = null;
	
	private List<String> senTrainIds = null;
	private List<String> senTestIds = null;
	private List<String> senHiddenIds = null;
	private Map<String, String[]> sampleIdDataMap = null; //Master map of all data
	
	private String[] cpgIds = null;
	//Instances
	private Instances trainInstances = null;
	private Instances testInstances = null;
	private Instances hiddenInstances = null;
	
	private List<SimulationDatasetEvaluation> simulationDatasetEvaluations = null;
	
	public SimulationDataset(String[] cpgIds, Map<String, String[]> sampleIdDataMap) {
		this.sampleIdDataMap = sampleIdDataMap;
		this.cpgIds = cpgIds;
		this.simulationDatasetEvaluations = new ArrayList<SimulationDatasetEvaluation>();
	}
	
	public void addSimulationDatasetEvaluation(SimulationDatasetEvaluation o) {
		this.simulationDatasetEvaluations.add(o);
	}
	
	public List<SimulationDatasetEvaluation> getSimulationDatasetEvaluations() {
		Collections.sort(this.simulationDatasetEvaluations);
		return this.simulationDatasetEvaluations;
	}
	
	public List<String> getAllergyTrainIds() {
		return allergyTrainIds;
	}
	public void setAllergyTrainIds(List<String> allergyTrainIds) {
		this.allergyTrainIds = allergyTrainIds;
	}
	public List<String> getAllergyTestIds() {
		return allergyTestIds;
	}
	public void setAllergyTestIds(List<String> allergyTestIds) {
		this.allergyTestIds = allergyTestIds;
	}
	public List<String> getAllergyHiddenIds() {
		return allergyHiddenIds;
	}
	public void setAllergyHiddenIds(List<String> allergyHiddenIds) {
		this.allergyHiddenIds = allergyHiddenIds;
	}
	public List<String> getSenTrainIds() {
		return senTrainIds;
	}
	public void setSenTrainIds(List<String> senTrainIds) {
		this.senTrainIds = senTrainIds;
	}
	public List<String> getSenTestIds() {
		return senTestIds;
	}
	public void setSenTestIds(List<String> senTestIds) {
		this.senTestIds = senTestIds;
	}
	public List<String> getSenHiddenIds() {
		return senHiddenIds;
	}
	public void setSenHiddenIds(List<String> senHiddenIds) {
		this.senHiddenIds = senHiddenIds;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Training\n");
		for (String id: this.allergyTrainIds) {
			sb.append(id +", ");
		}
		sb.append("\nTest\n");
		for (String id: this.allergyTestIds) {
			sb.append(id +", ");
		}
		sb.append("\nHidden\n");
		for (String id: this.allergyHiddenIds) {
			sb.append(id +", ");
		}
		return sb.toString();
	}

	public Instances getTrainInstances() {
		return trainInstances;
	}

	public void setTrainInstances(Instances trainInstances) {
		this.trainInstances = trainInstances;
	}

	public Instances getTestInstances() {
		return testInstances;
	}

	public void setTestInstances(Instances testInstances) {
		this.testInstances = testInstances;
	}

	public Instances getHiddenInstances() {
		return hiddenInstances;
	}

	public void setHiddenInstances(Instances hiddenInstances) {
		this.hiddenInstances = hiddenInstances;
	}
	
	

}
