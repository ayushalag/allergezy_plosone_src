package com.allergezy.fa.simulation.dataset;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.util.FoodAllergyFileConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * This class provides the value for a particular cpg and sampleid
 * @author Ayush Alag
 *
 */
public class DatasetValueReader implements FoodAllergyFileConstants {
	private static DatasetValueReader instance = null;
	private static final String fileName = GSE59999_Dataset_File;
	private Map<String,Map<String,String>> sampleCpgMap = null;
	private Map<String,Integer> cpgIndexMap = null;
	private String [] cpgIds = null;
	
	private DatasetValueReader() {
		initialize();
	}
	
	private void initialize() {
		String[] rows = InMemoryFileReader.getFileContentByRows(fileName);
		String [] terms = rows[0].split("\t");
		this.cpgIds = terms;
		this.cpgIndexMap = new HashMap<String,Integer>();
		int index = 0;
		for (String term: terms) {
			this.cpgIndexMap.put(term, index ++);
		}
		this.sampleCpgMap = new HashMap<String,Map<String,String>>();
		for (String row: rows) {
			terms = row.split("\t");
			Map<String, String> rowTerms = new HashMap<String,String>();
			int idx = 0;
			for (String term: terms) {
				rowTerms.put(cpgIds[idx ++], term);
			}
			this.sampleCpgMap.put(terms[0], rowTerms);
		}
	}
	
	/**
	 * Get instance
	 * @return
	 */
	public static DatasetValueReader getInstance() {
		if (instance == null) {
			synchronized(DatasetValueReader.class) {
				instance = new DatasetValueReader();
			}
		}
		return instance;
	}
	
	/**
	 * Get the sample cpg value
	 * @param sampleId
	 * @param cpg
	 * @return
	 */
	public String getSampleCpgValue(String sampleId, String cpg) {
		Map<String,String> sampleValues = this.sampleCpgMap.get(sampleId);
		if (sampleValues != null) {
			String value = sampleValues.get(cpg);
			return value;
		}
		return null;
	}
	
	
	public static void main(String [] args) {
		DatasetValueReader reader = DatasetValueReader.getInstance();
		String sampleId = "GSM1463328";
		String cpg = "cg06410630";
		String cValue = "0.901927427239818";
		String value = reader.getSampleCpgValue(sampleId, cpg);
		System.out.println(cpg + " " + value + " is " + (cValue.equals(value)));
		CPGSignatureReader sigReader = CPGSignatureReader.getInstance();
		List<String> cpgs = sigReader.getCPGSignature(0);
		for (String cpg1: cpgs) {
			System.out.println(cpg1 + " " + reader.getSampleCpgValue(sampleId, cpg1));
		}
		cpg = "cg07033513";
		value = reader.getSampleCpgValue(sampleId, cpg);
		System.out.println(cpg + " " + value + " is null =" + (cpg == null));
	}
	
}
