package com.allergezy.fa.simulation.dataset;

import java.util.ArrayList;

import com.allergezy.fa.util.FoodAllergyEnums.Algorithm;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;

public class SimulationDatasetEvaluation implements Comparable<SimulationDatasetEvaluation> {
	private SimulationDataset simulationDataset = null;
	private Algorithm algorithm;
	private ArrayList<Attribute> attributes;
	private Classifier classifier;
	// Models
	private Evaluation trainingModelEvaluation;
	private Evaluation testModelEvaluation;
	private Evaluation hiddenModelEvaluation;
	private Double[] hiddenPredictions;
	private Double[] hiddenErrors;
	private Double[] correctAnswers;

	public SimulationDatasetEvaluation(SimulationDataset simulationDataset, Algorithm algorithm,
			ArrayList<Attribute> attributes, Classifier classifier) {
		this.simulationDataset = simulationDataset;
		simulationDataset.addSimulationDatasetEvaluation(this);
		this.algorithm = algorithm;
		this.attributes = attributes;
		this.classifier = classifier;
		this.initialize();
	}

	private void initialize() {
		try {
			// Train the classifier with the training data
			classifier.buildClassifier(this.simulationDataset.getTrainInstances());
			//Evaluate the classifier
			trainingModelEvaluation = new Evaluation(this.simulationDataset.getTrainInstances());
			trainingModelEvaluation.evaluateModel(classifier, this.simulationDataset.getTrainInstances());
			testModelEvaluation = new Evaluation(this.simulationDataset.getTestInstances());
			testModelEvaluation.evaluateModel(classifier, this.simulationDataset.getTestInstances());
			hiddenModelEvaluation = new Evaluation(this.simulationDataset.getHiddenInstances());
			hiddenModelEvaluation.evaluateModel(classifier, this.simulationDataset.getHiddenInstances());
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public int compareTo(SimulationDatasetEvaluation s) {
		double diff = s.getTestModelEvaluation().correct() - this.getTestModelEvaluation().correct();
		if (diff == 0) {
			diff = s.getTrainingModelEvaluation().correct() - trainingModelEvaluation.correct();
		}
		if (diff > 0) {
			return 1;
		}
		return -1;
	}

	public SimulationDataset getSimulationDataset() {
		return simulationDataset;
	}

	public void setSimulationDataset(SimulationDataset simulationDataset) {
		this.simulationDataset = simulationDataset;
	}

	public Algorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}

	public Classifier getClassifier() {
		return classifier;
	}

	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}

	public Evaluation getTrainingModelEvaluation() {
		return trainingModelEvaluation;
	}

	public void setTrainingModelEvaluation(Evaluation trainingModelEvaluation) {
		this.trainingModelEvaluation = trainingModelEvaluation;
	}

	public Evaluation getTestModelEvaluation() {
		return testModelEvaluation;
	}

	public void setTestModelEvaluation(Evaluation testModelEvaluation) {
		this.testModelEvaluation = testModelEvaluation;
	}

	public Evaluation getHiddenModelEvaluation() {
		return hiddenModelEvaluation;
	}

	public void setHiddenModelEvaluation(Evaluation hiddenModelEvaluation) {
		this.hiddenModelEvaluation = hiddenModelEvaluation;
	}

	public Double[] getHiddenPredictions() {
		return hiddenPredictions;
	}

	public void setHiddenPredictions(Double[] hiddenPredictions) {
		this.hiddenPredictions = hiddenPredictions;
	}

	public Double[] getHiddenErrors() {
		return hiddenErrors;
	}

	public void setHiddenErrors(Double[] hiddenErrors) {
		this.hiddenErrors = hiddenErrors;
	}

	public Double[] getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(Double[] correctAnswers) {
		this.correctAnswers = correctAnswers;
	}

}
