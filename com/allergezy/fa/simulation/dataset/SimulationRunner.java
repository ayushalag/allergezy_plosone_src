package com.allergezy.fa.simulation.dataset;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.allergezy.fa.simulation.visitor.SimulationVisitor;
import com.allergezy.fa.simulation.visitor.SimulationVisitorImpl;
import com.allergezy.fa.simulation.visitor.SummarySimulationVisitorImpl;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.weka.WekaDatasetBuilder;
import com.allergezy.fa.weka.WekaHelper;

import weka.classifiers.Classifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This is the main runner class for a simulation
 * 
 * @author Ayush Alag
 *
 */
public class SimulationRunner implements FoodAllergyConstants {
	private WekaHelper wekaHelper = null;
	private WekaDatasetBuilder wekaDatasetBuilder = null;
	private DatasetValueReader valueReader = null;
	private static Algorithm[] algorithmsList = { Algorithm.MLP, Algorithm.RBF, Algorithm.DECISION_TREE,
			Algorithm.LOGISTIC_REGRESSION };

	public SimulationRunner() {
		this.wekaDatasetBuilder = WekaDatasetBuilder.getInstance();
		this.wekaHelper = new WekaHelper();
		this.valueReader = DatasetValueReader.getInstance();
	}

	public void simulate(List<String> cpgSignature, int numRuns, List<SimulationVisitor> visitors) {
		DatasetBuilder dataBuilder = DatasetBuilder.getInstance();

		double numCorrect = 0.;
		double numIncorrect = 0.;
		for (int i = 0; i < numRuns; i++) {
			SimulationDataset dataset = dataBuilder.createSimulationDataset(i);
			runSimulation(dataset, cpgSignature, visitors);

			// Get the best model on training set
			List<SimulationDatasetEvaluation> evals = dataset.getSimulationDatasetEvaluations();
			Collections.sort(evals);
			SimulationDatasetEvaluation e = evals.get(0);
			System.out.println("Iteration " + e.getHiddenModelEvaluation().correct() + " ======= "
					+ e.getHiddenModelEvaluation().incorrect());
			numCorrect += e.getHiddenModelEvaluation().correct();
			numIncorrect += e.getHiddenModelEvaluation().incorrect();
			double p = (numCorrect) / (numCorrect + numIncorrect);
			System.out.println("\t" + i + " ==> " + p + " " + numCorrect + ", " + numIncorrect);
			for (SimulationVisitor visitor : visitors) {
				visitor.visit(dataset, e);
			}
		}
		double per = (numCorrect) / (numCorrect + numIncorrect);
		System.out.println(numCorrect + " " + numIncorrect + " \t" + per);

		for (SimulationVisitor visitor : visitors) {
			try {
				visitor.cleanup(per);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	private void runSimulation(SimulationDataset dataSet, List<String> cpgSignature, List<SimulationVisitor> visitors) {
		ArrayList<Attribute> attributes = getFeatures(cpgSignature);
		createInstances(dataSet, cpgSignature, attributes);
	}

	/**
	 * Create instances from the dataset
	 * 
	 * @param dataSet
	 * @param cpgSignature
	 * @param attributes
	 * @return
	 */
	private void createInstances(SimulationDataset dataSet, List<String> cpgSignature,
			ArrayList<Attribute> attributes) {
		this.populateWekaInstances(dataSet, cpgSignature, attributes);
		for (Algorithm algorithm : this.algorithmsList) {
			// Get the classifier and build it using the training examples
			Classifier classifier = wekaHelper.getClassifier(algorithm);
			// Create the model and evaluate it
			SimulationDatasetEvaluation sde = new SimulationDatasetEvaluation(dataSet, algorithm, attributes,
					classifier);
		}
		// Get the best model on training set
		List<SimulationDatasetEvaluation> evals = dataSet.getSimulationDatasetEvaluations();
		/**
		 * for (SimulationDatasetEvaluation e : evals) { System.out.println("" +
		 * e.getHiddenModelEvaluation().correct() + " -- " +
		 * e.getHiddenModelEvaluation().incorrect() + " ==> " +
		 * e.getTestModelEvaluation().correct()); }
		 */

	}

	private void populateWekaInstances(SimulationDataset dataset, List<String> cpgSignature,
			ArrayList<Attribute> attributes) {
		Instances trainInstances = createInstances("Train", dataset.getAllergyTrainIds(), dataset.getSenTrainIds(),
				cpgSignature, attributes);
		Instances testInstances = createInstances("Test", dataset.getAllergyTestIds(), dataset.getSenTestIds(),
				cpgSignature, attributes);
		Instances hiddenInstances = createInstances("Hidden", dataset.getAllergyHiddenIds(), dataset.getSenHiddenIds(),
				cpgSignature, attributes);
		dataset.setTrainInstances(trainInstances);
		dataset.setTestInstances(testInstances);
		dataset.setHiddenInstances(hiddenInstances);
	}

	/**
	 * Create the instances for the provided set of sample Ids
	 * 
	 * @param name
	 * @param sampleIds
	 * @param cpgs
	 * @param attributes
	 * @param predValue
	 * @return
	 */
	private Instances createInstances(String name, List<String> allergySampleIds, List<String> senSampleIds,
			List<String> cpgs, ArrayList<Attribute> attributes) {

		Instances instances = new Instances(name, attributes, allergySampleIds.size() + senSampleIds.size());
		instances.setClassIndex(attributes.size() - 1); // index starts with 0
		for (String sampleId : allergySampleIds) {
			Instance instance = createInstance(instances, sampleId, cpgs, attributes, "1");
			instances.add(instance);
		}
		for (String sampleId : senSampleIds) {
			Instance instance = createInstance(instances, sampleId, cpgs, attributes, "0");
			instances.add(instance);
		}
		return instances;
	}

	private Instance createInstance(Instances instances, String sampleId, List<String> cpgs,
			ArrayList<Attribute> attributes, String predValue) {
		Instance instance = new DenseInstance(attributes.size());
		instance.setDataset(instances);
		int count = 0;
		for (String cpg : cpgs) {
			String term = this.valueReader.getSampleCpgValue(sampleId, cpg);
			//System.out.println(sampleId + "\t" + cpg + "\t" + term);
			double d = Double.parseDouble(term);
			instance.setValue(count++, d);
		}

		// Now populate the classification to be learnt
		instance.setValue(count, predValue);
		return instance;
	}

	/**
	 * Create a list of Attributes
	 * 
	 * @param cpgs
	 * @return
	 */
	protected ArrayList<Attribute> getFeatures(List<String> cpgs) {
		// The last column is predicted, rest are attributes of interest
		ArrayList<Attribute> allAttributes = new ArrayList<Attribute>(cpgs.size() + 1);
		// We can ignore the last class
		for (String cpg : cpgs) {
			Attribute attribue = new Attribute(cpg); // continuous
			// attribute
			allAttributes.add(attribue);
		}
		// The predicted attribute is nominal
		// Create list to hold nominal values "first", "second"
		List<String> values = new ArrayList<String>(2);
		values.add("" + Classification.Allergy.getValue());
		values.add("" + Classification.Sensitized.getValue());
		Attribute resultAttribute = new Attribute(Classification.Allergy.getName(), values);
		allAttributes.add(resultAttribute);
		return allAttributes;
	}

	public static void main(String[] args)  throws Exception {
		CPGSignatureReader sigReader = CPGSignatureReader.getInstance();
		int numRuns = 200;
		SimulationRunner runner = new SimulationRunner();
		SummarySimulationVisitorImpl summaryVisitor = new SummarySimulationVisitorImpl(SUMM_SIMUL_OUT_File);
		for (int i = 0; i < 2; i++) {
			List<String> cpgs = sigReader.getCPGSignature(i);
			for (String cpg : cpgs) {
		//		System.out.println(cpg);
			}
			List<SimulationVisitor> visitors = new ArrayList<SimulationVisitor>();
			String fileName = SIMUL_OUT_File + "-" + i + ".txt";
			SimulationVisitor visitor = new SimulationVisitorImpl(fileName);
			visitors.add(visitor);
			summaryVisitor.setup(i);
			visitors.add(summaryVisitor);

			try {
				runner.simulate(cpgs, numRuns, visitors);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
		summaryVisitor.allDone();
	}

}
