package com.allergezy.fa.simulation.dataset;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Class that reads the cpg signatures for simulation
 * 
 * @author Ayush Alag
 *
 */
public class CPGSignatureReader implements FoodAllergyConstants {
	private static CPGSignatureReader instance = null;
	private List<List<String>> cpgSignatures = null;
	private static final String fileName = CPG_SIGN_File;

	/**
	 * Constructor
	 */
	private CPGSignatureReader() {
		initialize();
	}

	/**
	 * Get instance method
	 * 
	 * @return
	 */
	public static CPGSignatureReader getInstance() {
		if (instance == null) {
			synchronized (CPGSignatureReader.class) {
				instance = new CPGSignatureReader();
			}
		}
		return instance;
	}

	/**
	 * Get the appropriate CPG signature
	 * 
	 * @param id
	 * @return
	 */
	public List<String> getCPGSignature(int id) {
		return this.cpgSignatures.get(id);
	}

	/**
	 * initialize
	 */
	private void initialize() {
		String[] rows = InMemoryFileReader.getFileContentByRows(this.fileName);
		this.cpgSignatures = new ArrayList<List<String>>(rows.length);
		for (int i = 0; i < rows.length; i++) {
			List<String> signature = new ArrayList<String>();
			this.cpgSignatures.add(signature);
			String[] terms = rows[i].split("\t");
			for (String term : terms) {
				if (term.startsWith("cg")) {
					signature.add(term);
				}
			}
		}
	}

	public static void main(String[] args) {
		CPGSignatureReader r = CPGSignatureReader.getInstance();
		List<String> signature = r.getCPGSignature(1);

		for (String term : signature) {
			System.out.println(term + ", ");
		}
		System.out.println();

	}
}
