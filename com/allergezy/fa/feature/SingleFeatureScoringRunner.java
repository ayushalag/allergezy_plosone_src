package com.allergezy.fa.feature;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.learn.resultwriter.impl.DetailedRunResultWriterImpl;
import com.allergezy.fa.learn.resultwriter.impl.SummaryRunResultWriterImpl;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.weka.WekaDatasetBuilder;
import com.allergezy.fa.weka.WekaHelper;

/**
 * This is the main runner class for analyzing the data for a single features. It iterates over the 8
 * epochs, varies the number of features, and algorithms
 * 
 * For multi attribute cases, Verify directory rewrote this logic and should be
 * used
 * 
 * @author Ayush Alag
 *
 */
public class SingleFeatureScoringRunner implements FoodAllergyConstants {

	/**
	 * Master method that iterates over all algorithms, varying the classifier,
	 * the number of features, and the epochs
	 * 
	 * @param wekaHelper
	 * @param resultWriters
	 */
	public void runAllCases(WekaHelper wekaHelper, List<ResultWriter> resultWriters,
			WekaDatasetBuilder wekaDatasetBuilder) {
		System.out.println("WekaHelper: " + wekaHelper.getClass());
		System.out.println("WekaDatasetBuilder: " + wekaDatasetBuilder.getClass());

		Algorithm[] algorithms = { Algorithm.MLP, Algorithm.LOGISTIC_REGRESSION, Algorithm.DECISION_TREE,
				Algorithm.RBF };
		
		int startCaseId = 1;
		int endCaseId = 11; // 664 max
		int startEpoch = 0;
		int endEpoch = 7;
		
		//Used to show progression
		int numCasesToRun = (endCaseId - startCaseId + 1) * (endEpoch - startEpoch + 1) * algorithms.length;
		int index = 1;

		//Iterate over all features
		for (int caseId = startCaseId; caseId <= endCaseId; caseId++) {
			for (int epochId = startEpoch; epochId <= endEpoch; epochId++) {
				for (Algorithm algorithm : algorithms) {
					try {
						wekaDatasetBuilder.executeLearningAnalysis(caseId, epochId, algorithm, resultWriters,
								wekaHelper);
						//Used to show progression
						System.out.println("Completed " + index++ + " run out of " + numCasesToRun);
					} catch (Throwable th) {
						th.printStackTrace();
					}
				}
			}

		}
	}

	/**
	 * Get the list of Result writers to write information about each iteration
	 * 
	 * @return
	 */
	protected List<ResultWriter> getResultWriter() {
		List<ResultWriter> resultWriters = new ArrayList<ResultWriter>();
		resultWriters.add(new DetailedRunResultWriterImpl(GSE59999_40_10_8_Single_Feature_Detail_Result, "addFeatures"));
		resultWriters.add(new SummaryRunResultWriterImpl(GSE59999_40_10_8_Single_Feature_Score_Result));
		return resultWriters;
	}

	/**
	 * Execute the case for iterating over all features to compute the feature
	 * score
	 */
	public void executeFeatureScoringCases() {
		WekaHelper wekaHelper =  new FeatureScoringWekaHelper();
		List<ResultWriter> resultWriters = new ArrayList<ResultWriter>();
		resultWriters.add(new DetailedRunResultWriterImpl(GSE59999_40_10_8_Single_Feature_Detail_Result, "featscore"));
		resultWriters.add(new SummaryRunResultWriterImpl(GSE59999_40_10_8_Single_Feature_Score_Result));
		runAllCases(wekaHelper, resultWriters, WekaDatasetBuilder.getInstance());
	}

	/**
	 * This starts with one case and then adds an additional feature as input to
	 * the classifier
	 */
	public void executeIncrementalFeatureScoringCases() {
		WekaHelper wekaHelper = new WekaHelper();
		List<ResultWriter> resultWriters = getResultWriter();
		runAllCases(wekaHelper, resultWriters, WekaDatasetBuilder.getInstance());
	}

	/**
	 * Main method to run the single feature scoring runner
	 * @param args
	 */
	public static void main(String[] args) {
		SingleFeatureScoringRunner wr = new SingleFeatureScoringRunner();
		wr.executeFeatureScoringCases();
	}

}
