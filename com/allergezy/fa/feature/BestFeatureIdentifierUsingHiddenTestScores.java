package com.allergezy.fa.feature;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.feature.result.FeatureRunResult;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Identifies the best feature from the single-feature scoring run
 * 
 * @author Ayush Alag
 *
 */
public class BestFeatureIdentifierUsingHiddenTestScores implements FoodAllergyConstants {

	/**
	 * Reads the scores from the GSE59999_40_10_8_Single_Feature_Score_Result file to 
	 * create a prioritized list of features
	 * 
	 */
	public void generateBestScorePerRun() {
		// Read the results from the runs
		String inputFileName =  GSE59999_40_10_8_Single_Feature_Score_Result;
		System.out.println("Reading file ..." + inputFileName);
		String[] rows = InMemoryFileReader.getFileContentByRows(inputFileName);
		//Populate a map with the Epoch scores
		Map<EpochCpg, List<FeatureRunResult>> map = new HashMap<EpochCpg, List<FeatureRunResult>>();
		for (String row : rows) {
			//Filter out non-data rows 
			if ((!row.startsWith("Id") && (row.length() > 1))) {
				//Parse it into object
				FeatureRunResult fr = getFeatureRunResult(row);
				//Inner class used as a key in Map
				EpochCpg ec = getEpochCpg(fr);
				List<FeatureRunResult> frList = map.get(ec);
				if (frList == null) {
					frList = new ArrayList<FeatureRunResult>();
					map.put(ec, frList);
				}
				frList.add(fr);
				map.put(ec, frList);
			}
		}
		// Now filter the run to the best score for the cpg and epoch
		Map<EpochCpg, FeatureRunResult> bestRunEpoch = new HashMap<EpochCpg, FeatureRunResult>();
		for (EpochCpg ec : map.keySet()) {
			List<FeatureRunResult> frList = map.get(ec);
			FeatureRunResult bestRun = null;
			for (FeatureRunResult fr : frList) {
				if (bestRun == null) {
					bestRun = fr;
				} else {
					if (fr.getPerTestCorrect() > bestRun.getPerTestCorrect()) {
						bestRun = fr;
					}
				}
				bestRunEpoch.put(ec, bestRun);
			}
		}
		//Write the  results
		List<FeatureRunResult> frList = writeResults(bestRunEpoch);
		Map<String, List<FeatureRunResult>> bestRun = new HashMap<String, List<FeatureRunResult>>();
		for (FeatureRunResult fr : frList) {
			List<FeatureRunResult> runs = bestRun.get(fr.getCpg());
			if (runs == null) {
				runs = new ArrayList<FeatureRunResult>();
				bestRun.put(fr.getCpg(), runs);
			}
			runs.add(fr);
		}
		// Consolidate to an average across all cpg runs
		List<FeatureRunResult> avgCPGRun = new ArrayList<FeatureRunResult>();
		for (List<FeatureRunResult> runs : bestRun.values()) {
			FeatureRunResult favg = getAverageRun(runs);
			avgCPGRun.add(favg);
		}
		Collections.sort(avgCPGRun);
		writeResults(avgCPGRun);
	}

	//Inner class for key to Map
	protected EpochCpg getEpochCpg(FeatureRunResult fr) {
		EpochCpg ec = new EpochCpg(fr.getCpg(), fr.getEpoch());
		return ec;
	}

	/**
	 * Write to the file GSE59999_40_10_8_BestFeatureScore_CPG_Result
	 * @param avgCPGRun
	 */
	private void writeResults(List<FeatureRunResult> avgCPGRun) {
		String fileName = GSE59999_40_10_8_BestFeatureScore_CPG_Result;
	//	fileName = GSE59999_40_10_8_FeatureCombination_CPGResult;
		System.out.println("Writing ... " + avgCPGRun.size() + " " + fileName);
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);

		try {
			for (FeatureRunResult fr : avgCPGRun) {
				bw.write(fr.toRow() + "\n");
				bw.flush();
			}
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}

	}

	/**
	 * Compute the average
	 * @param runs
	 * @return
	 */
	private FeatureRunResult getAverageRun(List<FeatureRunResult> runs) {
		FeatureRunResult first = runs.get(0);
		for (int i = 1; i < runs.size(); i++) {
			FeatureRunResult fr = runs.get(i);
			first.setHiddenROC(first.getHiddenROC() + fr.getHiddenROC());
			first.setPerHiddenCorrect(first.getPerHiddenCorrect() + fr.getPerHiddenCorrect());
			first.setPerHiddenIncorrect(first.getPerHiddenIncorrect() + fr.getPerHiddenIncorrect());

			first.setPerTrainCorrect(first.getPerTrainCorrect() + fr.getPerTrainCorrect());
			first.setPerTrainIncorrect(first.getPerTrainIncorrect() + fr.getPerTrainIncorrect());
			first.setTrainROC(first.getTrainROC() + fr.getTrainROC());

			first.setPerTestCorrect(first.getPerTestCorrect() + fr.getPerTestCorrect());
			first.setPerTestIncorrect(first.getPerTestIncorrect() + fr.getPerTestIncorrect());
			first.setTestROC(first.getTestROC() + fr.getTestROC());
		}
		first.divide(runs.size());
		return first;

	}

	/**
	 * Write results to file GSE59999_40_10_8_BestFeatureScore_CPGEpoch_Result
	 * @param bestRunEpoch
	 * @return
	 */
	private List<FeatureRunResult> writeResults(Map<EpochCpg, FeatureRunResult> bestRunEpoch) {
		String fileName = GSE59999_40_10_8_BestFeatureScore_CPGEpoch_Result;
	//	fileName = GSE59999_40_10_8_FeatureCombination_CaseResult;
		System.out.println("Writing epoch cpgs ... " + bestRunEpoch.size() + " " + fileName);
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		List<FeatureRunResult> frList = new ArrayList<FeatureRunResult>();
		for (FeatureRunResult fr : bestRunEpoch.values()) {
			frList.add(fr);
		}
		Collections.sort(frList);
		try {
			for (FeatureRunResult fr : frList) {
				bw.write(fr.toRow() + "\n");
				bw.flush();
			}
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
		return frList;
	}

	/**
	 * Convert to FeatureRunResult
	 * @param row
	 * @return
	 */
	private FeatureRunResult getFeatureRunResult(String row) {
		FeatureRunResult fr = new FeatureRunResult();
		String[] terms = row.split("\t");
		fr.setId(Integer.parseInt(terms[0]));
		fr.setCpg(terms[1]);
		fr.setAlgorithm(Algorithm.getAlgorithm(terms[2]));
		fr.setEpoch(Integer.parseInt(terms[3]));
		fr.setCpgId(Integer.parseInt(terms[4]));

		fr.setPerTrainCorrect(Double.parseDouble(terms[5]));
		fr.setPerTrainIncorrect(Double.parseDouble(terms[6]));
		fr.setTrainROC(Double.parseDouble(terms[7]));

		fr.setPerTestCorrect(Double.parseDouble(terms[8]));
		fr.setPerTestIncorrect(Double.parseDouble(terms[9]));
		fr.setTestROC(Double.parseDouble(terms[10]));

		fr.setPerHiddenCorrect(Double.parseDouble(terms[11]));
		fr.setPerHiddenIncorrect(Double.parseDouble(terms[12]));
		fr.setHiddenROC(Double.parseDouble(terms[13]));
		return fr;
	}

	/**
	 * Inner class to use as key in Map
	 *
	 */
	protected class EpochCpg {
		private String cpg;
		private int epoch;

		EpochCpg(String cpg, int epoch) {
			this.cpg = cpg;
			this.epoch = epoch;
		}

		public boolean equals(Object o) {
			EpochCpg ec = (EpochCpg) o;
			return this.cpg.equals(ec.cpg) && (epoch == ec.epoch);
		}

		public int hashCode() {
			return (cpg.hashCode()) + epoch * 1111111111;
		}
	}

	/**
	 * Runner for computation
	 * @param args
	 */
	public static void main(String[] args) {
		BestFeatureIdentifierUsingHiddenTestScores ti = new BestFeatureIdentifierUsingHiddenTestScores();
		ti.generateBestScorePerRun();
	}
}
