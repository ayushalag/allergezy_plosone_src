package com.allergezy.fa.feature.result;

import com.allergezy.fa.util.FoodAllergyEnums.Algorithm;

/**
 * Java bean class that implements the Comparable method for sorting
 * 
 * @author Ayush Alag
 *
 */
public class FeatureRunResult implements Comparable<FeatureRunResult> {
	private static final String delimiter = "\t";
	private int id;
	private String cpg;
	private int epoch;
	private int cpgId;
	private Algorithm algorithm;
	private double perTrainCorrect;
	private double perTrainIncorrect;
	private double trainROC;
	private double perTestCorrect;
	private double perTestIncorrect;
	private double testROC;
	private double perHiddenCorrect;
	private double perHiddenIncorrect;
	private double hiddenROC;
	private String cpgString = null;
	
	public void divide(int n) {
		this.perTrainCorrect /= n;
		this.perTrainIncorrect /= n;
		this.trainROC /= n;
		this.perTestCorrect /= n;
		this.perTestIncorrect /= n;
		this.testROC /= n;
		this.perHiddenCorrect /= n;
		this.perHiddenIncorrect /= n;
		this.hiddenROC /= n;
	}

	/**
	 * Method to sort the results
	 */
	public int compareTo(FeatureRunResult o) {
		double d = o.getPerHiddenCorrect() - this.perHiddenCorrect;
		if (d == 0.) {
			//Both have the same hidden accuracy, now look at test accuracy
			d = (o.getPerTestCorrect() - this.perTestCorrect)*100;
			return (int) d;
		}
		if (d > 0.) {
			return 1;
		}
		return -1;
	}

	/**
	 * Summarize it into a single row for printing
	 * @return
	 */
	public String toRow() {
		StringBuilder sb = new StringBuilder();
		sb.append(cpgId + delimiter);
		//sb.append(cpg + delimiter);
		sb.append(algorithm + delimiter);
		sb.append(epoch + delimiter);
		sb.append(id + delimiter);

		sb.append(perTrainCorrect + delimiter);
		sb.append(perTrainIncorrect + delimiter);
		sb.append(trainROC + delimiter);
		sb.append(perTestCorrect + delimiter);
		sb.append(perTestIncorrect + delimiter);
		sb.append(testROC + delimiter);
		sb.append(perHiddenCorrect + delimiter);
		sb.append(perHiddenIncorrect + delimiter);
		sb.append(hiddenROC + delimiter);
		if (this.cpgString != null) {
			sb.append(this.cpgString);
		}
		return sb.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCpg() {
		return cpg;
	}

	public void setCpg(String cpg) {
		this.cpg = cpg;
	}

	public int getEpoch() {
		return epoch;
	}

	public void setEpoch(int epoch) {
		this.epoch = epoch;
	}

	public int getCpgId() {
		return cpgId;
	}

	public void setCpgId(int cpgId) {
		this.cpgId = cpgId;
	}

	public Algorithm getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}

	public double getPerTrainCorrect() {
		return perTrainCorrect;
	}

	public void setPerTrainCorrect(double perTrainCorrect) {
		this.perTrainCorrect = perTrainCorrect;
	}

	public double getPerTrainIncorrect() {
		return perTrainIncorrect;
	}

	public void setPerTrainIncorrect(double perTrainIncorrect) {
		this.perTrainIncorrect = perTrainIncorrect;
	}

	public double getTrainROC() {
		return trainROC;
	}

	public void setTrainROC(double trainROC) {
		this.trainROC = trainROC;
	}

	public double getPerTestCorrect() {
		return perTestCorrect;
	}

	public void setPerTestCorrect(double perTestCorrect) {
		this.perTestCorrect = perTestCorrect;
	}

	public double getPerTestIncorrect() {
		return perTestIncorrect;
	}

	public void setPerTestIncorrect(double perTestIncorrect) {
		this.perTestIncorrect = perTestIncorrect;
	}

	public double getTestROC() {
		return testROC;
	}

	public void setTestROC(double testROC) {
		this.testROC = testROC;
	}

	public double getPerHiddenCorrect() {
		return perHiddenCorrect;
	}

	public void setPerHiddenCorrect(double perHiddenCorrect) {
		this.perHiddenCorrect = perHiddenCorrect;
	}

	public double getPerHiddenIncorrect() {
		return perHiddenIncorrect;
	}

	public void setPerHiddenIncorrect(double perHiddenIncorrect) {
		this.perHiddenIncorrect = perHiddenIncorrect;
	}

	public double getHiddenROC() {
		return hiddenROC;
	}

	public void setHiddenROC(double hiddenROC) {
		this.hiddenROC = hiddenROC;
	}

	public String getCpgString() {
		return cpgString;
	}

	public void setCpgString(String cpgString) {
		this.cpgString = cpgString;
	}
}
