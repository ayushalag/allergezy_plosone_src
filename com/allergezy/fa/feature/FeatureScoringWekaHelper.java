package com.allergezy.fa.feature;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.weka.WekaHelper;

import weka.core.Attribute;
import weka.core.Instance;

/**
 * Class to support Feature Scoring
 * @author Ayush Alag
 *
 */
public class FeatureScoringWekaHelper extends WekaHelper {
	/**
	 * Overwrites the add feature method
	 */
	protected void addFeature(int index, int numDimensions, List<String> headers,
			ArrayList<Attribute> allAttributes, String header) {
		if (index == numDimensions) {
			Attribute attribue = new Attribute(header); // continuous
														// attribute
			allAttributes.add(attribue);
		}
	}
	
	/**
	 * Sets the double value for the feature by parsing it
	 */
	protected void addFeatureValue(int count, int numFeatures, String term, Instance instance) {
		if (count == numFeatures) {
			double d = Double.parseDouble(term);
			instance.setValue(0, d);
		}
	}

}