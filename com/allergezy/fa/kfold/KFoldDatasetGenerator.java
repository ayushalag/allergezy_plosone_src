package com.allergezy.fa.kfold;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * This is the main file for dividing the samples into 8 hidden and 50 for training/test
 * There are 10 sub-cases that divide the 50 samples, such that each example is in 8 cases used 
 * for training and twice for testing. There are a total of 8 cases, which ensure that each sample
 * falls into at least one case where it is  hidden 
 * 
 * This allocation is done at random and allocation is written to GSE59999_40_10_8_DataFile in 
 * the case# directory
 * 
 * Output looks like
 * Case 0		
 * Training:	0	GSM1463328,GSM1463330,GSM1463342,GSM1463343,GSM1463344,GSM1463352,GSM1463354,GSM1463361,GSM1463362,GSM1463363,GSM1463365,GSM1463372,GSM1463373,GSM1463376,GSM1463378,GSM1463379,GSM1463384,GSM1463392,GSM1463394,GSM1463398,GSM1463331,GSM1463336,GSM1463338,GSM1463339,GSM1463341,GSM1463345,GSM1463349,GSM1463353,GSM1463367,GSM1463368,GSM1463371,GSM1463377,GSM1463385,GSM1463386,GSM1463388,GSM1463390,GSM1463391,GSM1463393,GSM1463396,GSM1463397,
 * Test:	0	GSM1463332,GSM1463337,GSM1463380,GSM1463387,GSM1463389,GSM1463340,GSM1463351,GSM1463357,GSM1463370,GSM1463383,
 * Hidden:	0	GSM1463334,GSM1463346,GSM1463347,GSM1463348,GSM1463350,GSM1463356,GSM1463358,GSM1463382,
 * 
 * This is the first step in our procedure
 * 
 * @author Ayush Alag
 *
 */
public class KFoldDatasetGenerator implements FoodAllergyConstants{	
		//Instance variable to ensure that data is read only once
		private String[] rows = null;

		/**
		 * Randomly allocate 8 examples to hidden set and remaining into test/train across 10 cases. 
		 * Iterate over all samples to ensure that each sample is in at least one case where it is hidden
		 * 
		 */
		public void generate50_8_8Datasets() throws Exception {
			BufferedWriter bw = FileUtil.getBufferedWriter(GSE59999_40_10_8_DataFile);
			int caseNumber = 0;
			List<String> allSampleNames = getAllSampleIds();
			//Get the master list of samples which are to be hidden each iteration
			List<List<String>> hiddenSamplesMasterList = generateHiddenSamples(allSampleNames);
			for (List<String> hiddenSamples: hiddenSamplesMasterList) {	
				List<String> allergySamples = new ArrayList<String>();
				for (int i = 0; i < 29; i++) {
					allergySamples.add(allSampleNames.get(i));
				}
				List<String> sensitizedSamples = new ArrayList<String>();
				for (int i = 29; i < 58; i++) {
					sensitizedSamples.add(allSampleNames.get(i));
				}
				//Remove the hidden samples from each list
				for (String id: hiddenSamples) {
					allergySamples.remove(id);
					sensitizedSamples.remove(id);
				}
				//Shuffle the list many times and allocate the top 20 from each to training and 10 for test
				for (int i = 0; i < 100; i ++) {
					Collections.shuffle(allergySamples);
					Collections.shuffle(sensitizedSamples);
				}
				
				String training = TRAINING_KEY + caseNumber + "\t";
				String test = TEST_KEY + caseNumber + "\t";
				String hidden = HIDDEN_KEY + caseNumber + "\t";
				
				bw.write("\nCase " + caseNumber + "\n");
				caseNumber ++;
				List<String> tempList1 = new ArrayList<String>();
				List<String> tempList2 = new ArrayList<String>();
				for (int i = 0; i < 20; i ++) {
					tempList1.add(allergySamples.get(i));
					tempList2.add(sensitizedSamples.get(i));
				}
				Collections.sort(tempList1);
				Collections.sort(tempList2);
				tempList1.addAll(tempList2);
				bw.write(training);
				for (String s: tempList1) {
					bw.write(s +",");
				}
				tempList1 = new ArrayList<String>();
				tempList2 = new ArrayList<String>();
				for (int i = 20; i < 25; i ++) {
					tempList1.add(allergySamples.get(i));
					tempList2.add(sensitizedSamples.get(i));
				}
				Collections.sort(tempList1);
				Collections.sort(tempList2);
				tempList1.addAll(tempList2);
				bw.write("\n" + test);
				for (String s: tempList1) {
					bw.write(s +",");
				}
				bw.write("\n" + hidden);
				for (String s: hiddenSamples) {
					bw.write(s +",");
				}
				bw.flush();
				
			}
			bw.close();
			
		}
		
		/**
		 * Gets random indexes
		 * @param low
		 * @param high
		 * @param n
		 * @return
		 */
		public static ArrayList<Integer> getRandomIndexes(int low, int high, int n) {
			ArrayList<Integer> outputOfRandomNumbers = new ArrayList<Integer>();
			for (int i = low; i < high+1; i++) {
				outputOfRandomNumbers.add(i);
			}
			Collections.shuffle(outputOfRandomNumbers);
			ArrayList<Integer> result = new ArrayList<Integer>();
			for (int i = 0; i < n; i++) {
				Integer num = (int) (Math.random() * (high - low + 1) + low);
				while (!(outputOfRandomNumbers.contains(num))) {
					num = (int) (Math.random() * (high - low + 1) + low);
				}
				result.add(num);
				int xcy = outputOfRandomNumbers.indexOf(num);
				outputOfRandomNumbers.remove(xcy);
			} 
	        Collections.sort(result);
	        System.out.println(result);
	        return result;
		}
		
		/**
		 * Helper method to select hidden cases
		 */
		public List<List<String>> generateHiddenSamples(List<String> allSampleNames) {
			List<List<String>> hiddenSamplesMasterList = new ArrayList<List<String>>();
			int numAdditional = 6;
			int numInHidden = 8;
			//There are 59 samples, select numAdditional samples at random to convert it into a list of 
			// 59 + numAdditional
			//This list will be split at random into (59 + numAdditional)/8 lists
			int numLists = (59 + numAdditional)/8;
			List<Integer> addAllergy = getRandomIndexes(0, 28, 3);
			List<Integer> addSen = getRandomIndexes(29, 57, 3);
			
			List<String> allergySamples = new ArrayList<String>();
			for (int i = 0; i < 29; i++) {
				allergySamples.add(allSampleNames.get(i));
			}
			List<String> sensitizedSamples = new ArrayList<String>();
			for (int i = 29; i < 58; i++) {
				sensitizedSamples.add(allSampleNames.get(i));
			}
			
			for (Integer id: addAllergy) {
				allergySamples.add(allSampleNames.get(id));
			}
			for (Integer id: addSen) {
				sensitizedSamples.add(allSampleNames.get(id));
			}
			
			for (int i =0; i < 100; i ++) {
				Collections.shuffle(allergySamples);
				Collections.shuffle(sensitizedSamples);
			}
			int index = 0;
			for (int i = 0; i < numLists; i ++) {
				List<String> hiddenSamplesId = new ArrayList<String>();
				hiddenSamplesMasterList.add(hiddenSamplesId);
				for (int j = 0; j < 4; j++) {
					hiddenSamplesId.add(allergySamples.get(index));
					hiddenSamplesId.add(sensitizedSamples.get(index));
					index ++;
				}	
				Collections.sort(hiddenSamplesId);
			}
			
			for (List<String> l: hiddenSamplesMasterList) {
				System.out.println("\nList: ");
				for (String s: l) {
					System.out.println(s + ", ");
				}
			}
			return hiddenSamplesMasterList;
		}

		/**
		 * Retrieves the training samples for the specified case
		 * @param c
		 * @return
		 */
		public String[] getTrainingCases(int c) {
			String key = TRAINING_KEY + c + "\t";
			return getCases(key);
		}

		/**
		 * Retrieves the test samples for the specified case
		 * @param c
		 * @return
		 */
		public String[] getTestCases(int c) {
			String key = TEST_KEY + c + "\t";
			return getCases(key);
		}

		/**
		 * Returns the list of hidden samples
		 * @return
		 */
		public String[] getHiddenCases() {
			String key = HIDDEN_KEY;
			return getCases(key);
		}

		/**
		 * Utility method to read test/training samples
		 * @param key
		 * @return
		 */
		private String[] getCases(String key) {
			if (rows == null) {
				rows = InMemoryFileReader.getFileContentByRows(GSE59999_40_10_8_DataFile);
			}

			for (String row : rows) {
				if (row.startsWith(key)) {
					row = row.substring(key.length());
					return row.split("\t");
				}
			}
			return null;
		}

		/**
		 * Get all sample ids
		 * @return
		 */
		protected List<String> getAllSampleIds() {
			List<String> names = getAllergyNames();
			names.addAll(getNames(FileUtil.readSenRowsFromFile()));
			return names;
		}

		/**
		 * Get allergy names
		 * @return
		 */
		protected List<String> getAllergyNames() {
			return getNames(FileUtil.readAllergyRowsFromFile());
		}
		
		/**
		 * Get allergy names
		 * @return
		 */
		protected List<String> getSensitizedNames() {
			return getNames(FileUtil.readSenRowsFromFile());
		}
		

		/**
		 * Get names
		 * @param rows
		 * @return
		 */
		private List<String> getNames(String[] rows) {
			List<String> names = new ArrayList<String>();
			boolean isFirst = true;
			for (String row : rows) {
				if (isFirst) {
					isFirst = false;
				} else {
					String[] terms = row.split("\t");
					names.add(terms[0]);
				}
			}
			return names;
		}

		public static void main(String[] args) throws Exception {
			KFoldDatasetGenerator g = new KFoldDatasetGenerator();
			g.generate50_8_8Datasets();
			
		}
		
		private void test1() {
			KFoldDatasetGenerator g = new KFoldDatasetGenerator();
			// g.generate50_10_8Datasets();
			int caseNum = 0;
			System.out.println("Training ..."  + "\t");
			String[] cases = g.getTrainingCases(caseNum);
			for (String c : cases) {
				System.out.println(c + "\t");
			}
			System.out.println();
			System.out.println("Test ..."  + "\t");
			cases = g.getTestCases(caseNum);
			for (String c : cases) {
				System.out.println(c + "\t");
			}
			cases = g.getHiddenCases();
			System.out.println("Hidden ..."  + "\t");
			for (String c : cases) {
				System.out.println(c + "\t");
			}
		}
}
