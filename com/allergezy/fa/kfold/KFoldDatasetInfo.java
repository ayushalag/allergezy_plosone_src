package com.allergezy.fa.kfold;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * This file uses the Singleton pattern to provide information about the test,
 * train, and hidden samples for each of the cases
 * 
 * @author Ayush Alag
 *
 */
public class KFoldDatasetInfo implements FoodAllergyConstants {
	private static KFoldDatasetInfo instance = null;
	private String[] dataRows = null;

	/**
	 * Private no args constructor reads the data about cases from file and
	 * stores in memory
	 */
	private KFoldDatasetInfo() {
		dataRows = InMemoryFileReader.getFileContentByRows(GSE59999_40_10_8_DataFile_Read);
	}

	/**
	 * Static method to get access to the Singleton
	 * 
	 * @return instance of KFoldDatasetInfo
	 */
	public static KFoldDatasetInfo getInstance() {
		if (instance == null) {
			synchronized (KFoldDatasetInfo.class) {
				instance = new KFoldDatasetInfo();
			}
		}
		return instance;
	}

	/**
	 * Efficiently retrieves the list of examples for the specified case
	 * 
	 * @param dataType
	 *            the kind of samples
	 * @param caseNumber
	 *            the specified case number
	 * @return
	 */
	public List<String> getExampleIds(DataType dataType, int caseNumber) {
		String key = dataType.getKey(caseNumber);
		List<String> retValue = new ArrayList<String>();
		for (String s : this.dataRows) {
			if (s.startsWith(key)) {
				String values = s.substring(key.length(), s.length()).trim();
				String[] terms = values.split(",");
				for (String term : terms) {
					if (term.startsWith("G")) {
						retValue.add(term);
					}
				}
			}
		}
		return retValue;
	}

	/**
	 * Test program
	 * @param args
	 */
	public static void main(String[] args) {
		KFoldDatasetInfo di = KFoldDatasetInfo.getInstance();
		for (int i = 0; i < 8; i++) {
			int caseNumber = i;
			for (DataType dataType : DataType.values()) {
				List<String> values = di.getExampleIds(dataType, caseNumber);
				System.out.print("\n" + dataType.getKey(caseNumber) + " [" + values.size() + "]:");
				for (String value : values) {
					System.out.print(value + ", ");
				}
			}
		}
	}
}
