package com.allergezy.fa.kfold.verify.analyze;

import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * Determines the best model [ML, RBF, DT, LR] for each epoch and averages the score 
 * across all 8 cases
 */
public class VerifyFeatureBestFeatureIdentifier extends MultiFeatureBestFeatureIdentifier
		implements FoodAllergyConstants {
	
	protected String getInputFileName() {
		return GSE59999_Verify_FeatureCombination_Result;
	}
	
	protected String getFileNameForCaseOverAllEpochs() {
		return GSE59999_40_10_8_FeatureCombination_CPGResult;
	}
	
	protected String getFileNameForCaseOverEachEpochs() {
		return GSE59999_40_10_8_FeatureCombination_CaseResult;
	}
	
	public static void main(String[] args) {
		VerifyFeatureBestFeatureIdentifier ti = new VerifyFeatureBestFeatureIdentifier();
		ti.generateBestScorePerRun();
	}
}
