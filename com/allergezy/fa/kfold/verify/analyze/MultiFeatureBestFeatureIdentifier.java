package com.allergezy.fa.kfold.verify.analyze;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.feature.result.FeatureRunResult;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.FoodAllergyEnums.Algorithm;
import com.allergezy.fa.util.InMemoryFileReader;

public class MultiFeatureBestFeatureIdentifier  implements FoodAllergyConstants{
	
	protected String getInputFileName() {
		return GSE59999_Verify_FeatureCombination_Result;
	}
	public void generateBestScorePerRun() {
		// Read the results from the runs
		String inputFileName =  getInputFileName();
		System.out.println("Reading from file " + inputFileName);
		String[] rows = InMemoryFileReader.getFileContentByRows(inputFileName);
		Map<EpochCase, List<FeatureRunResult>> map = new HashMap<EpochCase, List<FeatureRunResult>>();
		for (String row : rows) {
			// System.out.println(row);
			if ((!row.startsWith("Id") && (row.length() > 1))) {
				FeatureRunResult fr = getFeatureRunResult(row);
				EpochCase ec = getEpochCpg(fr);
				List<FeatureRunResult> frList = map.get(ec);
				if (frList == null) {
					frList = new ArrayList<FeatureRunResult>();
					map.put(ec, frList);
				}
				frList.add(fr);
				map.put(ec, frList);
			}
		}
		
		
		// Now filter the run to the best score for the case and epoch
		Map<EpochCase, FeatureRunResult> bestRunEpoch = new HashMap<EpochCase, FeatureRunResult>();

		for (EpochCase ec : map.keySet()) {
			List<FeatureRunResult> frList = map.get(ec);
			FeatureRunResult bestRun = null;
			for (FeatureRunResult fr : frList) {
				if (bestRun == null) {
					bestRun = fr;
				} else {
					if (fr.getPerTestCorrect() > bestRun.getPerTestCorrect()) {
						bestRun = fr;
					}
				}
			}
			bestRunEpoch.put(ec, bestRun);
		}
		List<FeatureRunResult> bestRunsPerEpochCase = writeResults(bestRunEpoch);
		
		Map<Integer, List<FeatureRunResult>> averageRun = new HashMap<Integer, List<FeatureRunResult>>();
		for (FeatureRunResult fr : bestRunEpoch.values()) {
			List<FeatureRunResult> runs = averageRun.get(fr.getCpgId());
			if (runs == null) {
				runs = new ArrayList<FeatureRunResult>();
				averageRun.put(fr.getCpgId(), runs);
			}
			runs.add(fr);
		}
		// Consolidate to an average across all cpg runs
		List<FeatureRunResult> avgCPGRun = new ArrayList<FeatureRunResult>();
		for (List<FeatureRunResult> runs : averageRun.values()) {
			FeatureRunResult favg = getAverageRun(runs);
			avgCPGRun.add(favg);
		}
		Collections.sort(avgCPGRun);
		writeResults(avgCPGRun);
	}

	protected EpochCase getEpochCpg(FeatureRunResult fr) {
		EpochCase ec = new EpochCase(fr.getCpgId(), fr.getEpoch());
		return ec;
	}
	
	protected String getFileNameForCaseOverAllEpochs() {
		return GSE59999_40_10_8_FeatureCombination_CPGResult;
	}

	private void writeResults(List<FeatureRunResult> avgCPGRun) {
		String fileName = getFileNameForCaseOverAllEpochs(); 
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		System.out.println("Writing to file " +  fileName);
		try {
			for (FeatureRunResult fr : avgCPGRun) {
				bw.write(fr.toRow() + "\n");
				bw.flush();
			}
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}

	}

	private FeatureRunResult getAverageRun(List<FeatureRunResult> runs) {
		FeatureRunResult first = runs.get(0);
		for (int i = 1; i < runs.size(); i++) {
			
			FeatureRunResult fr = runs.get(i);
			System.out.println(first.getPerHiddenCorrect() + " + " + fr.getPerHiddenCorrect());
			first.setHiddenROC(first.getHiddenROC() + fr.getHiddenROC());
			first.setPerHiddenCorrect(first.getPerHiddenCorrect() + fr.getPerHiddenCorrect());
			first.setPerHiddenIncorrect(first.getPerHiddenIncorrect() + fr.getPerHiddenIncorrect());

			first.setPerTrainCorrect(first.getPerTrainCorrect() + fr.getPerTrainCorrect());
			first.setPerTrainIncorrect(first.getPerTrainIncorrect() + fr.getPerTrainIncorrect());
			first.setTrainROC(first.getTrainROC() + fr.getTrainROC());

			first.setPerTestCorrect(first.getPerTestCorrect() + fr.getPerTestCorrect());
			first.setPerTestIncorrect(first.getPerTestIncorrect() + fr.getPerTestIncorrect());
			first.setTestROC(first.getTestROC() + fr.getTestROC());
		}
		first.divide(runs.size());
		return first;

	}
	
	protected String getFileNameForCaseOverEachEpochs() {
		return GSE59999_40_10_8_FeatureCombination_CaseResult;
	}

	private List<FeatureRunResult> writeResults(Map<EpochCase, FeatureRunResult> bestRunEpoch) {
		String fileName = getFileNameForCaseOverEachEpochs();
		System.out.println("Writing to file " +  fileName);
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		List<FeatureRunResult> frList = new ArrayList<FeatureRunResult>();
		for (FeatureRunResult fr : bestRunEpoch.values()) {
			frList.add(fr);
		}
		Collections.sort(frList);
		try {
			for (FeatureRunResult fr : frList) {
				bw.write(fr.toRow() + "\n");
				bw.flush();
			}
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
		return frList;
	}

	private FeatureRunResult getFeatureRunResult(String row) {
		FeatureRunResult fr = new FeatureRunResult();
		String[] terms = row.split("\t");
		fr.setId(Integer.parseInt(terms[0]));
		fr.setCpg(terms[1]);
		fr.setAlgorithm(Algorithm.getAlgorithm(terms[2]));
		fr.setEpoch(Integer.parseInt(terms[3]));
		fr.setCpgId(Integer.parseInt(terms[4]));

		fr.setPerTrainCorrect(Double.parseDouble(terms[5]));
		fr.setPerTrainIncorrect(Double.parseDouble(terms[6]));
		fr.setTrainROC(Double.parseDouble(terms[7]));

		fr.setPerTestCorrect(Double.parseDouble(terms[8]));
		fr.setPerTestIncorrect(Double.parseDouble(terms[9]));
		fr.setTestROC(Double.parseDouble(terms[10]));

		fr.setPerHiddenCorrect(Double.parseDouble(terms[11]));
		fr.setPerHiddenIncorrect(Double.parseDouble(terms[12]));
		fr.setHiddenROC(Double.parseDouble(terms[13]));
		if (terms.length > 14) {
			StringBuilder sb = new StringBuilder();
			for (int i = 14; i < terms.length; i ++) {
				sb.append("\t" + terms[i]);
			}
			fr.setCpgString(sb.toString());
		}
		return fr;
	}

	protected class EpochCase {
		private int caseId;
		private int epoch;

		EpochCase(int caseId, int epoch) {
			this.caseId = caseId;
			this.epoch = epoch;
		}

		public boolean equals(Object o) {
			EpochCase ec = (EpochCase) o;
			return (this.caseId == ec.getCaseId()) && (epoch == ec.epoch);
		}
		
		public int getCaseId() {
			return this.caseId;
		}

		public int hashCode() {
			return (caseId) + epoch * 1111111111;
		}
	}

	public static void main(String[] args) {
		MultiFeatureBestFeatureIdentifier ti = new MultiFeatureBestFeatureIdentifier();
		ti.generateBestScorePerRun();
	}
}
