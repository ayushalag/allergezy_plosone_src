package com.allergezy.fa.kfold.verify;

import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.learn.resultwriter.impl.BestModelForCasesResultWriterImpl;
import com.allergezy.fa.learn.resultwriter.impl.DetailedRunResultWriterImpl;
import com.allergezy.fa.learn.resultwriter.impl.NonAllergicResultWriter;
import com.allergezy.fa.learn.resultwriter.impl.SummaryRunResultWriterImpl;
import com.allergezy.fa.multifeature.reader.VerifyCasesReader;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.weka.WekaDatasetBuilder;
import com.allergezy.fa.weka.WekaHelper;

public class VerifyWekaDatasetRunner implements FoodAllergyConstants {
	/**
	 * Master method that iterates over all algorithms, varying the classifier, the
	 * number of features, and the epochs
	 * 
	 * @param wekaHelper
	 * @param visitors
	 */
	public void runAllCases(WekaHelper wekaHelper, List<ResultWriter> visitors, WekaDatasetBuilder wekaDatasetBuilder) {
		// The different algorithms to be tried
		//Algorithm[] algorithms = { Algorithm.MLP, Algorithm.LOGISTIC_REGRESSION, Algorithm.DECISION_TREE,
		//		Algorithm.RBF };
		Algorithm[] algorithms = {  Algorithm.DECISION_TREE};
		
		// Reads the examples to be run
		VerifyCasesReader casesReader = VerifyCasesReader.getInstance();
		List<List<String>> examples = casesReader.getExamples();
		// Number of epochs is fixed to 8
		int startEpoch = 0;
		int endEpoch = 7;

		int numCasesToRun = (examples.size()) * (endEpoch - startEpoch + 1) * algorithms.length;
		int index = 1;
		int caseNumber = 0;
		// Itereate over all examples
		for (List<String> example : examples) {
			Integer exId = Integer.parseInt(example.get(0));
			// For each example run all epochs
			for (int epochId = startEpoch; epochId <= endEpoch; epochId++) {
				// For each case run all the algorithms
				for (Algorithm algorithm : algorithms) {
					try {
						wekaDatasetBuilder.executeLearningAnalysis(exId, epochId, algorithm, visitors, wekaHelper);

						System.out.println(
								"Completed " + example.get(0) + " " + index++ + " run out of " + numCasesToRun);
					} catch (Throwable th) {
						th.printStackTrace();
					}
				}
			}
			caseNumber++;
		}
		// Cleanup the result writers
		for (ResultWriter resultWriter : visitors) {
			resultWriter.cleanup();
		}
	}

	public void executeFeatureScoringCases() {
		VerifyMultiFeatureWekaHelper helper = new VerifyMultiFeatureWekaHelper();
		// Creates a list of visitors to process the data
		List<ResultWriter> resultWriters = getVisitors();
		runAllCases(helper, resultWriters, new MultiWekaDatasetBuilder(helper));
	}

	private List<ResultWriter> getVisitors() {
		List<ResultWriter> resultWriters = new ArrayList<ResultWriter>();
		// Detail of each run
		resultWriters.add(new DetailedRunResultWriterImpl(GSE59999_Verify_DetailCombination_Result, "case"));
		// Best model for each case -- summary majority prediction by voting
		resultWriters.add(new BestModelForCasesResultWriterImpl(GSE59999_Verify_BestAnalyzerOut_CaseResult));
		// Summary for each case
		resultWriters.add(new SummaryRunResultWriterImpl(GSE59999_Verify_FeatureCombination_Result));
		// NonAllergicResultWriter for each case
		resultWriters.add(new NonAllergicResultWriter(GSE59999_Verify_NonAllergic_Result));

		return resultWriters;
	}

	public static void main(String[] args) {
		VerifyWekaDatasetRunner wr = new VerifyWekaDatasetRunner();
		wr.executeFeatureScoringCases();
	}

}