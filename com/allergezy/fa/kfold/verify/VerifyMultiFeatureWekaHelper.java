package com.allergezy.fa.kfold.verify;

/**
 * Helper class to create the different datasets for creating multiple cpg signatures
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.kfold.KFoldDatasetInfo;
import com.allergezy.fa.multifeature.reader.VerifyCasesReader;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;
import com.allergezy.fa.weka.WekaHelper;

import weka.core.Attribute;

public class VerifyMultiFeatureWekaHelper extends WekaHelper implements FoodAllergyConstants {
	private String[] allergyRows = null;
	private String[] senRows = null;
	private Map<String, Integer> allergyMap = null;
	private Map<String, Integer> senMap = null;

	/**
	 * Constructor Initializes the datastructures used to cache sample data and
	 * mapping cpgs to column ids for fast lookup later
	 */
	public VerifyMultiFeatureWekaHelper() {
		allergyRows = InMemoryFileReader.getFileContentByRows(GSE59999AllergyFile);
		senRows = InMemoryFileReader.getFileContentByRows(GSE59999SenFile);
		// Map the cpgs to their position in the reference dataset
		allergyMap = new HashMap<String, Integer>();
		String[] cpgs = allergyRows[0].split("\t");
		int index = 0;
		for (String cpg : cpgs) {
			allergyMap.put(cpg, index);
			index++;
		}
		senMap = new HashMap<String, Integer>();
		cpgs = senRows[0].split("\t");
		index = 0;
		for (String cpg : cpgs) {
			senMap.put(cpg, index);
			index++;
		}
	}

	/**
	 * Get the features that will be used to learn
	 * 
	 * @param headers
	 * @param caseId
	 * @return
	 */
	protected ArrayList<Attribute> getFeatures(List<String> headers, int caseId) {
		// Col 1 is name of sample - ignore
		// The last column is predicted, rest are attributes of interest
		ArrayList<Attribute> allAttributes = new ArrayList<Attribute>(headers.size());
		// We can ignore the last class
		int index = 0;
		for (String header : headers) {
			addFeature(index, caseId, headers, allAttributes, header);
			index++;
		}
		return allAttributes;
	}

	protected void addFeature(int index, int caseId, List<String> headers, ArrayList<Attribute> allAttributes,
			String header) {
		if ((index > 0)) {
			Attribute attribue = new Attribute(header); // continuous
														// attribute
			allAttributes.add(attribue);
		}
	}

	protected List<String> getCPGs(int caseNumber) {
		// First get the list of cpgs for this particular case
		List<String> cpgs = VerifyCasesReader.getInstance().getExampleById(caseNumber);
		return cpgs;
	}

	/**
	 * Overwrite the default method to dynamically create the dataset using the
	 * appropriate cases and cpgs that have been assigned to this case
	 * 
	 * @param dataType
	 *            type of dataset Train, Test, and Hidden
	 * @param caseNumber
	 *            is the id of the case to be run -- combination id
	 * @param epochId
	 *            --this should be between 0 - 7
	 * @return
	 */
	public List<List<String>> getDataValues(DataType dataType, int epochId, int caseNumber) {
		List<List<String>> retDataset = new ArrayList<List<String>>();
		// First get the list of cpgs for this particular case
		List<String> cpgs = getCPGs(caseNumber);
		retDataset.add(cpgs);
		// cpgs.add(Classification.Allergy.name());

		// Now get the list of examples that need to be added for this epoch
		// Which examples go to which dataset is the function of epoch and
		// independent of
		// caseNumber
		List<String> examples = KFoldDatasetInfo.getInstance().getExampleIds(dataType, epochId);
		Map<String, String> examplesMap = new HashMap<String, String>();
		for (String example : examples) {
			examplesMap.put(example, example);
		}
		// Iterate over all rows of data and check if its a valid example. Add
		// to list if it is
		for (String row : allergyRows) {
			String[] terms = row.split("\t");
			if ((terms.length > 100) && (examplesMap.containsKey(terms[0]))) {
				List<String> exampleRow = new ArrayList<String>();
				retDataset.add(exampleRow);
				// System.out.println(terms[0] + " " + terms.length);
				// Now add the cpgs
				exampleRow.add(terms[0]); // Example id
				for (String cpg : cpgs) {
					if (cpg.startsWith("c")) {
						int index = allergyMap.get(cpg);
						// System.out.println(cpg + " -> " + index);
						exampleRow.add(terms[index]);
					}
				}
				exampleRow.add("" + Classification.Allergy.getValue());
			}
		}
		for (String row : senRows) {
			String[] terms = row.split("\t");
			if ((terms.length > 100) && (examplesMap.containsKey(terms[0]))) {
				List<String> exampleRow = new ArrayList<String>();
				retDataset.add(exampleRow);
				// Now add the cpgs
				exampleRow.add(terms[0]); // Example id
				for (String cpg : cpgs) {
					if (cpg.startsWith("c")) {
						int index = senMap.get(cpg);
						exampleRow.add(terms[index]);
					}
				}
				exampleRow.add("" + Classification.Sensitized.getValue());
			}
		}
		return retDataset;
	}

	//Main
	public static void main(String[] args) {
		VerifyMultiFeatureWekaHelper h = new VerifyMultiFeatureWekaHelper();
		int caseId = 100;
		int epochId = 0;
		List<List<String>> examples = h.getDataValues(DataType.TRAIN, caseId, epochId);
		for (List<String> example : examples) {
			System.out.println();
			for (String value : example) {
				System.out.print(value + ", ");
			}
		}
	}
}
