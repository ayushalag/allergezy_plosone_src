package com.allergezy.fa.kfold.verify;

import java.util.List;

import com.allergezy.fa.weka.WekaDatasetBuilder;

public class MultiWekaDatasetBuilder extends WekaDatasetBuilder {
	private VerifyMultiFeatureWekaHelper wekaHelper = null;
	
	public MultiWekaDatasetBuilder(VerifyMultiFeatureWekaHelper wekaHelper) {
		this.wekaHelper = wekaHelper;
	}
	
	/**
	 * Returns the data values for the specified case.
	 */
	protected List<List<String>> getDataValues(DataType dataType, int epochId, int caseNumber) {
		 return this.wekaHelper.getDataValues(dataType, epochId,caseNumber);
	}
}
