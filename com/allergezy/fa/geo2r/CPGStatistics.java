package com.allergezy.fa.geo2r;

import java.util.ArrayList;
import java.util.List;

/**
 * CPG Statistics class to analyze the distribution of CPGs across the eight cases
 * 
 * @author Ayush Alag
 *
 */
public class CPGStatistics  implements Comparable<CPGStatistics> {
	private static final int MAX_SCORE = 100;
	private String cpgName = null;
	private String geneName = null;
	private List<CPGPosition> positions = null;
	private int score = 0;
	
	/**
	 * Constructor
	 * @param cpgName
	 */
	public CPGStatistics(String cpgName) {
		this.cpgName = cpgName;
		this.positions = new ArrayList<CPGPosition>();
		this.geneName = CPGAnnotations.getInstance().getGeneMapping(this.cpgName);
	}
	
	/**
	 * Equals method to allow this to be used as key in a hashmap
	 * @param o
	 * @return
	 */
	public boolean equals(CPGStatistics o) {
		return this.cpgName.equals(o.getCPGName());
	}
	
	/**
	 * Hashcode
	 */
	public int hashCode() {
		return this.cpgName.hashCode();
	}
	
	/**
	 * Comparable interface for sorting 
	 */
	public int compareTo(CPGStatistics o) {
		return o.getScore() - this.score;
	}
	
	/**
	 * Add a position for this CPG
	 * @param position
	 */
	public void addPosition(int position) {
		CPGPosition p = new CPGPosition(this.cpgName, position);
		this.positions.add(p);
		//Each position adds to the score (100 - position)
		this.score += (MAX_SCORE - position);
	}
	
	public int getScore() {
		return this.score;
	}
	
	public String getCPGName() {
		return this.cpgName;
	}
	
	public String getGeneName() {
		return this.geneName;
	}
	
	public List<CPGPosition> getPositions() {
		return this.positions;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder(this.cpgName+ " [" + this.score + "-" );
		for (CPGPosition p: this.positions) {
			sb.append(p.position + ",");
		}
		sb.append("] -- {" + this.geneName + "}");
		return sb.toString();
	}
	
	public class CPGPosition implements Comparable<CPGPosition> {
		private String cpgName = null;
		private int position = 0;
		
		CPGPosition(String cpgName, int position) {
			this.cpgName = cpgName;
			this.position = position;
		}
		
		public int compareTo(CPGPosition o) {
			return (this.position - o.position);
		}
		
		public int getPosition() {
			return this.position;
		}
		
	}

}
