package com.allergezy.fa.geo2r;

import java.util.HashMap;
import java.util.Map;

import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

/**
 * Singleton instance that maps CPG points to Genes. Master file is at
 * GSE59999_Geo2R_to_CPG_Annotations
 * 
 * A row maps to "ID" "adj.P.Val" "P.Value" "t" "B" "logFC" "Coordinate_36"
 * "UCSC_RefGene_Name" "UCSC_CpG_Islands_Name" "Regulatory_Feature_Name"
 * "RANGE_START" "RANGE_END" "RANGE_GB"
 * 
 * @author Ayush Alag
 *
 */
public class CPGAnnotations implements FoodAllergyConstants {
	private static CPGAnnotations instance = null;
	private Map<String, String> cpgToGeneMap = null;

	/**
	 *  Private constructor
	 */
	private CPGAnnotations() {
		this.initialize();
	}

	/**
	 * Static method to get instance
	 * @return
	 */
	public static CPGAnnotations getInstance() {
		if (instance == null) {
			synchronized (CPGAnnotations.class) {
				instance = new CPGAnnotations();
			}
		}
		return instance;
	}

	/**
	 * Initialize -- read the annotations file GSE59999_Geo2R_to_CPG_Annotations into memory
	 */
	private void initialize() {
		String[] rows = InMemoryFileReader.getFileContentByRows(GSE59999_Geo2R_to_CPG_Annotations);
		this.cpgToGeneMap = new HashMap<String, String>();
		for (String row : rows) {
			String[] terms = row.split("\t");
			String cpg = terms[0].replaceAll("\"", "");
			String gene = terms[7].replaceAll("\"", "");
			// System.out.println(cpg + "-> " + gene);
			this.cpgToGeneMap.put(cpg, gene);
		}
	}

	/**
	 * Get the gene annotation
	 * @param cpg
	 * @return
	 */
	public String getGeneMapping(String cpg) {
		return this.cpgToGeneMap.get(cpg);
	}

	/**
	 * Test method
	 * @param args
	 */
	public static void main(String[] args) {
		String[] cpgs1 = { "cg24854095", "cg10336671", "cg17825100", "cg04958411", "cg12474695", "cg10022873",
				"cg05569767" };
		String[] cpgs = { "cg06410630", "cg06669701", "cg06628000", "cg10461264", "cg18988685", "cg24616138",
				"cg27027230", "cg00936790", "cg14414100", "cg00939931", "cg06116095", "cg02788266", "cg03068039",
				"cg25890092", "cg19287711", "cg07033513", "cg07060505", "cg26963090" };

		CPGAnnotations ann = CPGAnnotations.getInstance();
		for (String cpg : cpgs) {
			System.out.println(cpg + " -> " + ann.getGeneMapping(cpg));
		}
	}
}
