package com.allergezy.fa.geo2r;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;
/**
 * This Singleton class provides APIs to access CPGs for each of the 8 epochs
 * 
 * @author Ayush Alag
 *
 */
public class Geo2rCpgInfo implements FoodAllergyConstants {
	private static final int MAX_NUM_CPG = 99;
	private static Geo2rCpgInfo instance = null;
	private List<String>[] cpgsLists = null;

	/**
	 * Private constructor
	 */
	private Geo2rCpgInfo() {
		initialize();
	}

	/**
	 * Public static method to get instance
	 * 
	 * @return instance of Geo2rCpgInfo
	 */
	public static Geo2rCpgInfo getInstance() {
		if (instance == null) {
			synchronized (Geo2rCpgInfo.class) {
				instance = new Geo2rCpgInfo();
			}
		}
		return instance;
	}

	/**
	 * Read the CpG data in for each of the eight cases. This has been produced by Geo2R 
	 */
	private void initialize() {
		String[] rows = InMemoryFileReader.getFileContentByRows(GSE59999_40_10_8_Geo2R_cpg);
		this.cpgsLists = (ArrayList<String>[]) new ArrayList[8];
		for (int i = 0; i < this.cpgsLists.length; i++) {
			this.cpgsLists[i] = new ArrayList<String>();
		}

		int num = rows.length;
		if (num > MAX_NUM_CPG) {
			num = MAX_NUM_CPG;
		}
		for (int i = 0; i < num; i++) {
			String row = rows[i];
			// System.out.println(row);
			String[] terms = row.trim().split("\t");
			if (terms.length == 8) {
				int j = 0;
				for (String term : terms) {
					List<String> cpgList = this.cpgsLists[j++];
					cpgList.add(term);
				}
			}
		}
	}

	/**
	 * Retrieve the cpg list for the specified case
	 * @param caseNumber should be between [0 and 7]
	 * 
	 */
	public List<String> getOrderedCPGs(int caseNumber) {
		return this.cpgsLists[caseNumber];
	}

	/**
	 * Analyses the distribution of CPGs across the different cases
	 * 
	 * @param numCPGsPerCase
	 *            max number of cpgs per case to be considered
	 */
	public List<CPGStatistics> getCPGStatistics(int numCPGsPerCase) {
		Map<String, CPGStatistics> map = new HashMap<String, CPGStatistics>();
		// Create the statistics for the 8 cases
		for (int i = 0; i < 8; i++) {
			List<String> cpgs = getOrderedCPGs(i);
			for (int j = 0; j < numCPGsPerCase; j++) {
				String cpgName = cpgs.get(j);
				CPGStatistics stat = map.get(cpgName);
				if (stat == null) {
					stat = new CPGStatistics(cpgName);
					map.put(cpgName, stat);
				}
				stat.addPosition(j);
			}
		}
		// Sort them by scores
		List<CPGStatistics> stats = new ArrayList<CPGStatistics>();
		for (CPGStatistics stat : map.values()) {
			stats.add(stat);
		}
		Collections.sort(stats);
		writeCPGStats(stats);
		return stats;
	}

	/**
	 * Write the computed statistics to file GSE59999_40_10_8_FeatureScore_Geo2R
	 * @param stats
	 */
	private void writeCPGStats(List<CPGStatistics> stats) {
		try {
			String fileName = GSE59999_40_10_8_FeatureScore_Geo2R;
			BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
			bw.write("Id\tCPG\tGene\tScore\tPosition\n");
			int index = 1;
			for (CPGStatistics stat : stats) {
				StringBuilder sb = new StringBuilder("" + index++);
				sb.append("\t" + stat.getCPGName() + "\t" + stat.getGeneName() + "\t");
				sb.append(stat.getScore() + "\t");
				for (CPGStatistics.CPGPosition pos : stat.getPositions()) {
					sb.append(pos.getPosition() + ",");
				}
				bw.write(sb.toString() + "\n");
				bw.flush();
			}
			bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	/**
	 * Main method to compute the statistics
	 * @param args
	 */
	public static void main(String[] args) {
		Geo2rCpgInfo gi = Geo2rCpgInfo.getInstance();
		// gi.testAccess(gi);
		gi.testStatistics(gi);
	}

	private void testStatistics(Geo2rCpgInfo gi) {
		int[] numCPGs = { 99 };
		for (int num : numCPGs) {
			List<CPGStatistics> stats = gi.getCPGStatistics(num);
			System.out.println("Case: " + num + " .... " + stats.size());
			for (CPGStatistics stat : stats) {
				System.out.print(stat + ", ");
			}
			System.out.println("\n");
		}
	}

	private void testAccess(Geo2rCpgInfo gi) {
		for (int i = 0; i < 8; i++) {
			List<String> cpgs = gi.getOrderedCPGs(i);
			System.out.println(i + ". [" + cpgs.size() + "] " + cpgs.get(90));
		}
	}
}
