package com.allergezy.fa.learn.resultwriter;

import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.util.FoodAllergyConstants;

/**
 * Interface to implement logic based on two callbacks during the learning process. 
 * Once after each example and second at the end of an epoch
 * 
 * @author Ayush Alag
 *
 */
public interface ResultWriter extends FoodAllergyConstants {
	/**
	 * Call back for every epoch
	 * @param epochResultHolder
	 */
	public void visit(EpochResultHolder epochResultHolder);

	/**
	 * Call back at the end of all the epochs
	 */
	public void cleanup();
}
