package com.allergezy.fa.learn.resultwriter.impl;

import java.util.ArrayList;

import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FoodAllergyConstants;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;

/**
 * Base class for implementations of ResultWrite interface
 * 
 * @author Ayush Alag
 *
 */
public class ResultWriterImpl implements FoodAllergyConstants, ResultWriter {

	/**
	 * Expanded form of the visit method
	 * @param numDimensions
	 * @param caseNumber
	 * @param algorithm
	 * @param attributes
	 * @param classifer
	 * @param trainingInstances
	 * @param testInstances
	 * @param hiddenInstances
	 * @param trainingModelEvaluation
	 * @param testModelEvaluation
	 * @param hiddenModelEvaluation
	 * @param epochResultHolder
	 */
	protected void visit(int numDimensions, int caseNumber, Algorithm algorithm, 
			ArrayList<Attribute> attributes, Classifier classifer, Instances trainingInstances,
			Instances testInstances,Instances hiddenInstances,Evaluation trainingModelEvaluation,
			Evaluation testModelEvaluation,
			Evaluation hiddenModelEvaluation,EpochResultHolder epochResultHolder) {
		
	}
	
	/**
	 * Visit call back
	 */
	public void visit(EpochResultHolder epochResultHolder) {
		visit(epochResultHolder.getNumDimensions(), epochResultHolder.getCaseNumber(),
				epochResultHolder.getAlgorithm(), epochResultHolder.getAttributes(), 
				epochResultHolder.getClassifier(), epochResultHolder.getTrainingInstances(),
				epochResultHolder.getTestInstances(), epochResultHolder.getHiddenInstances(),
				epochResultHolder.getTrainingModelEvaluation(),epochResultHolder.getTestModelEvaluation(),
				epochResultHolder.getHiddenModelEvaluation(),epochResultHolder);
	}
	
	/**
	 * Call back at the end of an epoch
	 */
	public void cleanup() {
		
	}
}
