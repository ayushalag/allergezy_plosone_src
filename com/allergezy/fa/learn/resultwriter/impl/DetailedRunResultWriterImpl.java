package com.allergezy.fa.learn.resultwriter.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;

import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * This class writes out detailed information for a run to a file
 * 
 * @author Ayush Alag
 *
 */
public class DetailedRunResultWriterImpl extends ResultWriterImpl implements ResultWriter, FoodAllergyConstants {
	private String fileNamePrefix = null;
	private String baseFileNamePrefix = null;
	
	public DetailedRunResultWriterImpl(String baseFileNamePrefix, String fileNamePrefix) {
		this.fileNamePrefix = fileNamePrefix;
		this.baseFileNamePrefix = baseFileNamePrefix;
	}
	
	public void visit(int numDimensions, int caseNumber, Algorithm algorithm, ArrayList<Attribute> attributes,
			Classifier classifer, Instances trainingInstances, Instances testInstances, Instances hiddenInstances,
			Evaluation trainingModelEvaluation, Evaluation testModelEvaluation,
			Evaluation hiddenModelEvaluation,EpochResultHolder epochResultHolder) {
		StringBuilder sb = new StringBuilder(
				"Epoch: " + caseNumber + " Number Dimensions: " + numDimensions + " Algorithm: " + algorithm + "\n");
		for (Attribute attribute : attributes) {
			// sb.append(attribute + "\n");
		}
		try {
			writeModelInformation(sb, trainingModelEvaluation, DataType.TRAIN, classifer, trainingInstances);
			writeModelInformation(sb, testModelEvaluation, DataType.TEST, classifer, testInstances);
			writeModelInformation(sb, hiddenModelEvaluation, DataType.HIDDEN, classifer, hiddenInstances);
			writeToFile(sb, numDimensions, caseNumber, algorithm);
			if (classifer instanceof J48) {
				sb.append((((J48) classifer).graph()));
			}
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private void writeModelInformation(StringBuilder sb, Evaluation modelEvaluation, DataType dataType,
			Classifier classifier, Instances dataset) throws Exception {
		sb.append(dataset);
		// sb.append(classifier);
		sb.append(dataType.getName() + "\n");
		sb.append(modelEvaluation.toSummaryString());
		// System.out.println(modelEvaluation.toSummaryString());
		double correctClassification = modelEvaluation.correct();
		double incorrectClassification = modelEvaluation.incorrect();
		double pctCorrect = modelEvaluation.pctCorrect();
		// double pctIncorrect = modelEvaluation.pctIncorrect();
		ArrayList<Prediction> predictions = modelEvaluation.predictions();
		String s = "\t" + correctClassification + "\t" + incorrectClassification + "\t" + pctCorrect;
		sb.append(s + "\n");
		// Enumerate on all instances
		for (Enumeration<Instance> e = dataset.enumerateInstances(); e.hasMoreElements();) {
			Instance instance = e.nextElement();
			double prediction = classifier.classifyInstance(instance);
			sb.append("" + prediction);
			double[] distribution = classifier.distributionForInstance(instance);
			sb.append("\t" + "[" + distribution[0] + "] ");
		}

	}
	
	protected String getFileName(int caseNumber, int numDimensions, Algorithm algorithm) {
		return baseFileNamePrefix
				 + "case" + caseNumber + File.separator + algorithm
				+ File.separator + fileNamePrefix + numDimensions + ".txt";
	}

	private void writeToFile(StringBuilder sb, int numDimensions, int caseNumber, Algorithm algorithm)
			throws Exception {
		String fileName = getFileName(caseNumber, numDimensions, algorithm);
		//System.out.println("Writing to file ... " + fileName);
		BufferedWriter bw = FileUtil.getBufferedWriter(fileName);
		bw.write(sb.toString());
		bw.flush();
		bw.close();
	}
}
