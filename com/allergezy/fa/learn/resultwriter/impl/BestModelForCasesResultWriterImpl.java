package com.allergezy.fa.learn.resultwriter.impl;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FileUtil;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;

/**
 * This visitor computes the prediction by looking at the majority prediction of 
 * the odd-numbered of 
 * @author Ayush Alag
 *
 */
public class BestModelForCasesResultWriterImpl extends ResultWriterImpl implements ResultWriter {
	private static final String delimiter = "\t";
	private BufferedWriter bw = null;
	// Store the best record for each case and each epoch
	private Map<RunEpochKey, List<EpochResultHolder>> runResultsMap;
	// Store the best record for each epoch
	private Map<Integer, List<EpochResultHolder>> caseResultsMap;
	private List<List<EpochResultHolder>> allCasesList = null;
	private Integer previousCaseId = null;
	private Integer previousEpochId = null;
	private RunEpochKey previousRunEpochKey = null;

	/**
	 * Constructor
	 * @param fileName
	 */
	public BestModelForCasesResultWriterImpl(String fileName) {
		this.bw = FileUtil.getBufferedWriter(fileName);
		this.runResultsMap = new HashMap<RunEpochKey, List<EpochResultHolder>>();
		this.caseResultsMap = new HashMap<Integer, List<EpochResultHolder>>();
		this.allCasesList = new ArrayList<List<EpochResultHolder>>();
		System.out.println("BestModelForCasesResultWriterImpl::Writing to file ... " + fileName);
	}

	/**
	 * Callback after each iteration
	 * 
	 */
	public void visit(int caseId, int epochId, Algorithm algorithm, ArrayList<Attribute> attributes,
			Classifier classifier, Instances trainingInstances, Instances testInstances, Instances hiddenInstances,
			Evaluation trainingModelEvaluation, Evaluation testModelEvaluation, Evaluation hiddenModelEvaluation,
			EpochResultHolder epochResultHolder) {
		RunEpochKey runEpochKey = new RunEpochKey(caseId, epochId);
		List<EpochResultHolder> results = this.runResultsMap.get(runEpochKey);
		if (results == null) {
			results = new ArrayList<EpochResultHolder>();
			this.runResultsMap.put(runEpochKey, results);
		}
		results.add(epochResultHolder);
		String s = "";
		// Caseid and epochid
		if ((this.previousRunEpochKey != null) && (!this.previousRunEpochKey.equals(runEpochKey))) {
			results = this.runResultsMap.get(previousRunEpochKey);
			Collections.sort(results);
			EpochResultHolder bestResult = results.get(0);
			s = "Best for epoch:" + previousCaseId + ", " + previousEpochId + "," + bestResult.getAlgorithm() + ", "
					+ bestResult.getHiddenModelEvaluation().correct() + "\n";
			// write(s);
		}
		if ((this.previousEpochId != null) && (this.previousEpochId != epochId)) {
			List<EpochResultHolder> bestCasesForEpoch = caseResultsMap.get(previousCaseId);
			if (bestCasesForEpoch == null) {
				bestCasesForEpoch = new ArrayList<EpochResultHolder>();
				this.caseResultsMap.put(previousCaseId, bestCasesForEpoch);
			}
			bestCasesForEpoch.add(results.get(0));
			s = "Changing epochs.. and adding : " + previousCaseId + "," + previousEpochId + ","
					+ results.get(0).getHiddenModelEvaluation().correct() + " " + results.get(0).getAlgorithm() + "\n";
			write(s);
		}
		if ((this.previousCaseId != null) && (this.previousCaseId != caseId)) {
			System.out.println("Changing cases from .. " + this.previousCaseId + " to " + caseId);
			List<EpochResultHolder> bestCasesForEpoch = caseResultsMap.get(previousCaseId);
			this.allCasesList.add(bestCasesForEpoch);
		}

		this.previousCaseId = caseId;
		this.previousEpochId = epochId;
		this.previousRunEpochKey = runEpochKey;
	}

	private void write(String s) {
		try {
			bw.write(s);
			bw.flush();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	/**
	 * Called at the end when all the runs have been done
	 * @throws Exception
	 */
	 private void processAllMergedCases() throws Exception {
		// For completeness, we need to add the last row
		List<EpochResultHolder> results = this.runResultsMap.get(previousRunEpochKey);
		Collections.sort(results);		
		
		List<EpochResultHolder> bestCasesForEpoch = caseResultsMap.get(previousCaseId);
		this.allCasesList.add(bestCasesForEpoch);
		bestCasesForEpoch.add(results.get(0));
		
		String s = "Adding the last epoch case: " + previousCaseId + "," + previousEpochId + ","
				+ bestCasesForEpoch.get(0).getHiddenModelEvaluation().correct() + " "
				+ bestCasesForEpoch.get(0).getAlgorithm() + "\n";
		write(s);

		int n = this.allCasesList.size(); // There are n classifiers

		write("processAllMergedCases = : " + n + "\n");
		for (List<EpochResultHolder> ls : this.allCasesList) {
			for (EpochResultHolder erh : ls) {
//				write(erh.getNumDimensions()  + "->" + erh.getCaseNumber() + ", " +
//						getInfo(erh.getHiddenPredictions()) + "\n");
			}
		}

		// use odd number of classifier 1, 3, 5, .., n to vote for each case
	//	for (int i = 0; i < n; i = i ++) {
			for (int i = 0; i < n; i = i + 2) {
			int numVoters = i + 1;
		//	write("Final merging with numVoters = : " + numVoters + "\n");
			double accuracySum = 0.;
			for (int k = 0; k < 8; k++) {
				List<Double[]> predictions = new ArrayList<Double[]>();
				Double[] correct = this.allCasesList.get(0).get(0).getCorrectAnswers();
				//write("Epoch: " + k + " " + getInfo(correct) + "\n");
				for (int j = 0; j < numVoters; j++) {
					// Do this for each epoch
					List<EpochResultHolder> epochs = this.allCasesList.get(j);
					EpochResultHolder epoch = epochs.get(k);
					predictions.add(epoch.getHiddenPredictions());
				}
				accuracySum += computeAccuracy(predictions, correct);
			}
			double finalAccuracy = accuracySum / 8.;
			bw.write("Num voters: " + numVoters + this.delimiter + finalAccuracy + "\n");
		}
		bw.flush();
	}
	
	private String getInfo(Double [] ds) {
		StringBuilder sb = new StringBuilder();
		for (Double d: ds) {
			sb.append(d + ", ");
		}
		return sb.toString();
	}

	private double computeAccuracy(List<Double[]> predictions, Double[] correct) {
		double[] sum = new double[correct.length];
		for (Double[] items : predictions) {
			int i = 0;
			for (Double item : items) {
			//	System.out.println(item + " " + (item == null) + " " + i + " " + sum[i]);
				sum[i++] += item;
			}
		}
		int numCorrect = 0;
		double threshold = predictions.size() / 2.;
	//	write("Threshold:" + threshold + " " );
		for (double d: sum) {
	//		write(d +",");
		}
	//	write("\n");
		
		for (int i = 0; i < correct.length; i++) {
			if (correct[i] > 0) {
				if (sum[i] >= threshold) {
					numCorrect++;
				}
			} else {
				if (sum[i] < threshold) {
					numCorrect++;
				}
			}
		}
		double r = numCorrect / (1. * correct.length);
	//	write("NumCorrect= " + numCorrect + " out of " + correct.length + " r=" + r + "\n");
		return r;
	}

	public void cleanup() {
		try {
			this.processAllMergedCases();
			this.bw.flush();
			this.bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private class RunEpochKey implements Comparable<RunEpochKey> {
		private int epochNumber;
		private int caseId;
		private int hashCode;

		public RunEpochKey(int caseId, int epochNumber) {
			this.epochNumber = epochNumber;
			this.caseId = caseId;
			this.hashCode = ("5" + this.caseId + "^" + this.epochNumber).hashCode();
		}

		public int hashCode() {
			return this.hashCode;
		}

		public boolean equals(Object o) {
			return (this.hashCode == o.hashCode());
		}

		public int compareTo(RunEpochKey o) {
			if (caseId != o.caseId) {
				return (caseId - o.caseId);
			}
			// It's the same case, now sort it by epochNumber
			return (this.epochNumber - o.epochNumber);
		}
	}

}
