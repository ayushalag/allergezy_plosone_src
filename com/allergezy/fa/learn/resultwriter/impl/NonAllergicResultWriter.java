package com.allergezy.fa.learn.resultwriter.impl;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.List;

import com.allergezy.fa.dataset.NonAllergicDatasetCreator;
import com.allergezy.fa.kfold.verify.VerifyMultiFeatureWekaHelper;
import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.FoodAllergyConstants;
import com.allergezy.fa.util.InMemoryFileReader;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

public class NonAllergicResultWriter extends ResultWriterImpl implements ResultWriter, FoodAllergyConstants {
	private static final String delimiter = "\t";
	private BufferedWriter bw = null;
	private int count = 0;
	private NonAllergicDatasetCreator datasetCreator = null;
	private VerifyMultiFeatureWekaHelper helper = null;

	public NonAllergicResultWriter(String fileName) {
		this.datasetCreator = NonAllergicDatasetCreator.getInstance();
		this.helper = new VerifyMultiFeatureWekaHelper();
		// Read the content and write back
		String[] rows = InMemoryFileReader.getFileContentByRows(fileName);
		this.bw = FileUtil.getBufferedWriter(fileName);
		System.out.println("Writing to file ... " + fileName);
		try {

			if (rows.length > 0) {
				if (!rows[0].startsWith("Id")) {
					bw.write("Id\tCPG\tAlgorithm\tEpoch\tnumDimensions\ttrainCorrect\t trainIncorrect\t"
							+ "trainROC\ttestCorrect\ttestInCorrect\ttestROC\t"
							+ "hiddenCorrect\thiddenIncorrect\thiddenROC"
							+ "nonAllergicCorrect\tnonAllergicIncorrect\tnonAllergicROC" + "\n");
				}
				for (String row : rows) {
					bw.write(row + "\n");
					bw.flush();
				}
				count = rows.length;
			} else {
				bw.write("Id\tCPG\tFeature\tAlgorithm\tEpoch\tnumDimensions\ttrainCorrect\t trainIncorrect\t"
						+ "trainROC\ttestCorrect\ttestInCorrect\ttestROC\t"
						+ "hiddenCorrect\thiddenIncorrect\thiddenROC"
						+ "nonAllergicCorrect\tnonAllergicIncorrect\tnonAllergicROC" + "\n");
			}
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public void visit(int numDimensions, int caseNumber, Algorithm algorithm, ArrayList<Attribute> attributes,
			Classifier classifer, Instances trainingInstances, Instances testInstances, Instances hiddenInstances,
			Evaluation trainingModelEvaluation, Evaluation testModelEvaluation, Evaluation hiddenModelEvaluation,
			EpochResultHolder epochResultHolder) {
		Evaluation nonAllergicEvaluation = evaluateNonAllergicSamples(caseNumber, attributes, classifer);

		System.out.println(hiddenModelEvaluation.confusionMatrix());
		System.out.println(hiddenModelEvaluation.toSummaryString());
		System.out.println(hiddenModelEvaluation.weightedAreaUnderROC());
		try {
			for (int j = 0; j < hiddenInstances.numInstances(); j++) {
				Instance instance = hiddenInstances.get(j);
				double output = classifer.classifyInstance(instance);
				double correct = instance.value(attributes.size() - 1);
				double diff = Math.abs(output - correct);
				System.out.println(output + " diff=" + diff + " correct=" + (diff < 1));
				System.out.println(instance);
			}
		} catch (Throwable th) {
			th.printStackTrace();
		}

		StringBuilder sb = new StringBuilder();

		sb.append(count++ + delimiter + getAttributesString(attributes) + delimiter + algorithm + delimiter + caseNumber
				+ delimiter + numDimensions + delimiter + trainingModelEvaluation.pctCorrect() + delimiter
				+ trainingModelEvaluation.pctIncorrect() + delimiter + trainingModelEvaluation.weightedAreaUnderROC());
		sb.append(delimiter + testModelEvaluation.pctCorrect() + delimiter + testModelEvaluation.pctIncorrect()
				+ delimiter + testModelEvaluation.weightedAreaUnderROC());
		sb.append(delimiter + hiddenModelEvaluation.pctCorrect() + delimiter + hiddenModelEvaluation.pctIncorrect()
				+ delimiter + hiddenModelEvaluation.weightedAreaUnderROC());
		sb.append(delimiter + nonAllergicEvaluation.pctCorrect() + delimiter + nonAllergicEvaluation.pctIncorrect()
				+ delimiter + nonAllergicEvaluation.weightedAreaUnderROC());
		for (Attribute attribute : attributes) {
			sb.append(delimiter + attribute.name());
		}
		try {
			bw.write(sb.toString() + "\n");
			bw.flush();
		} catch (Throwable th) {
			th.printStackTrace();
		}

	}

	private String getAttributesString(ArrayList<Attribute> attributes) {
		StringBuilder sb = new StringBuilder();
		for (Attribute attribute : attributes) {
			sb.append(attribute.name() + ",");
		}
		return sb.toString();
	}

	private Evaluation evaluateNonAllergicSamples(int caseId, ArrayList<Attribute> attributes, Classifier classifier) {
		try {
			String[] cpgs = new String[attributes.size() - 1];
			int i = 0;
			for (Attribute attribute : attributes) {
				String cpg = attribute.name();
				if (cpg.startsWith("cg")) {
					cpgs[i++] = attribute.name();
				}
			}

			List<List<String>> dataValues = this.datasetCreator.getDataValuesForCPG(cpgs);

			Instances nonAllergicInstances = helper.createDataset(DataType.HIDDEN.getName(), dataValues, attributes,
					attributes.size());
			Evaluation evaluation = new Evaluation(nonAllergicInstances);
			evaluation.evaluateModel(classifier, nonAllergicInstances);
			System.out.println(evaluation.confusionMatrix());
			System.out.println(evaluation.toSummaryString());
			System.out.println(evaluation.weightedAreaUnderROC());

			// Set the hiddenPredictions and hiddenErrors
			try {
				for (int j = 0; j < nonAllergicInstances.numInstances(); j++) {
					Instance instance = nonAllergicInstances.get(j);
					double output = classifier.classifyInstance(instance);
					double correct = instance.value(attributes.size() - 1);
					double diff = Math.abs(output - correct);
					System.out.println(output + " diff=" + diff + " correct=" + (diff < 1));
					System.out.println(instance);
				}
			} catch (Throwable th) {
				th.printStackTrace();
			}

			return evaluation;
		} catch (Throwable th) {
			th.printStackTrace();
			return null;
		}
	}

	public void cleanup() {
		try {
			this.bw.flush();
			this.bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}
}
