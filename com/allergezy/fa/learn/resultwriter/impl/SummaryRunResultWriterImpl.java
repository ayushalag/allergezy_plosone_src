package com.allergezy.fa.learn.resultwriter.impl;

import java.io.BufferedWriter;
import java.util.ArrayList;

import com.allergezy.fa.learn.EpochResultHolder;
import com.allergezy.fa.learn.resultwriter.ResultWriter;
import com.allergezy.fa.util.FileUtil;
import com.allergezy.fa.util.InMemoryFileReader;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instances;

public class SummaryRunResultWriterImpl extends ResultWriterImpl implements ResultWriter {
	private static final String delimiter = "\t";
	private BufferedWriter bw = null;
	private int count = 0;

	public SummaryRunResultWriterImpl(String fileName) {
	// Read the content and write back
		String[] rows = InMemoryFileReader.getFileContentByRows(fileName);
		this.bw = FileUtil.getBufferedWriter(fileName);
		System.out.println("Writing to file ... " + fileName);
		try {
			
			if (rows.length > 0) {
				if (!rows[0].startsWith("Id")) {
					bw.write("Id\tCPG\tAlgorithm\tEpoch\tnumDimensions\ttrainCorrect\t trainIncorrect\t"
							+ "trainROC\ttestCorrect\ttestInCorrect\ttestROC\t"
							+ "hiddenCorrect\thiddenIncorrect\thiddenROC\n");
				}
				for (String row : rows) {
					bw.write(row + "\n");
					bw.flush();
				}
				count = rows.length;
			} else {
				bw.write("Id\tCPG\tFeature\tAlgorithm\tEpoch\tnumDimensions\ttrainCorrect\t trainIncorrect\t"
						+ "trainROC\ttestCorrect\ttestInCorrect\ttestROC\t"
						+ "hiddenCorrect\thiddenIncorrect\thiddenROC\n");
			}
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}
	
	private String getAttributesString(ArrayList<Attribute> attributes) {
		StringBuilder sb = new StringBuilder() ;
		for (Attribute attribute: attributes) {
			sb.append(attribute.name() + ",");
		}
		return sb.toString();
	}

	//Write result for each case
	public void visit(int numDimensions, int caseNumber, Algorithm algorithm, ArrayList<Attribute> attributes,
			Classifier classifer, Instances trainingInstances, Instances testInstances, Instances hiddenInstances,
			Evaluation trainingModelEvaluation, Evaluation testModelEvaluation, 
			Evaluation hiddenModelEvaluation,EpochResultHolder epochResultHolder) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(count++ + delimiter + getAttributesString(attributes)  + delimiter + algorithm + delimiter + 
				caseNumber + delimiter + numDimensions + delimiter
				+ trainingModelEvaluation.pctCorrect() + delimiter + trainingModelEvaluation.pctIncorrect() + delimiter
				+ trainingModelEvaluation.weightedAreaUnderROC());
		sb.append(delimiter + testModelEvaluation.pctCorrect() + delimiter + testModelEvaluation.pctIncorrect() + delimiter
				+ testModelEvaluation.weightedAreaUnderROC());
		sb.append(delimiter + hiddenModelEvaluation.pctCorrect() + delimiter + hiddenModelEvaluation.pctIncorrect() + delimiter
				+ hiddenModelEvaluation.weightedAreaUnderROC());
		for (Attribute attribute: attributes) {
			sb.append(delimiter + attribute.name());
		}
		try {
			bw.write(sb.toString() + "\n");
			bw.flush();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	public void cleanup() {
		try {
			this.bw.flush();
			this.bw.close();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

}
