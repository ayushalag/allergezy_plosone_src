package com.allergezy.fa.learn;

import java.util.ArrayList;

import com.allergezy.fa.util.FoodAllergyEnums.Algorithm;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Evaluation;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Stores information for an Epoch.
 * It's a Comparable Java Bean class
 * 
 * @author Ayush Alag
 *
 */
public class EpochResultHolder implements Comparable<EpochResultHolder>{
	private int numDimensions;
	private int caseNumber;
	private Algorithm algorithm;
	private ArrayList<Attribute> attributes;
	private Classifier classifier; 
	private Instances trainingInstances;
	private Instances testInstances; 
	private Instances hiddenInstances;
	private Evaluation trainingModelEvaluation;
	private Evaluation testModelEvaluation;
	private Evaluation hiddenModelEvaluation;
	private Double [] hiddenPredictions;
	private Double [] hiddenErrors;
	private Double [] correctAnswers;
	
	public EpochResultHolder(int numDimensions, int caseNumber, Algorithm algorithm, ArrayList<Attribute> attributes,
			Classifier classifier, Instances trainingInstances, Instances testInstances, Instances hiddenInstances,
			Evaluation trainingModelEvaluation, Evaluation testModelEvaluation, 
			Evaluation hiddenModelEvaluation) {
	  this.numDimensions = numDimensions;
	  this.caseNumber = caseNumber;
	  this.algorithm = algorithm;
	  this.attributes = attributes;
	  this.classifier = classifier;
	  this.trainingInstances = trainingInstances;
	  this.testInstances = testInstances;
	  this.hiddenInstances = hiddenInstances; 
	  this.trainingModelEvaluation = trainingModelEvaluation;
	  this.testModelEvaluation = testModelEvaluation;
	  this.hiddenModelEvaluation = hiddenModelEvaluation;
	  int n = hiddenInstances.size();
	  this.hiddenPredictions = new Double[n];
	  this.hiddenErrors = new Double[n];
	  this.correctAnswers = new Double[n];
	  //Set the hiddenPredictions and hiddenErrors
	  try {
	  for (int i = 0; i < hiddenInstances.numInstances(); i++) {
			Instance instance = hiddenInstances.get(i);
			double output = classifier.classifyInstance(instance);
			double correct = instance.value(attributes.size() - 1);
			double diff = Math.abs(output - correct);
			this.hiddenPredictions[i] = output;
			this.hiddenErrors[i] = diff;
			this.correctAnswers[i]=correct;
		}
	  } catch (Throwable th) {
		  th.printStackTrace();
	  }
	}
	
	public int getNumDimensions() {
		return numDimensions;
	}
	public void setNumDimensions(int numDimensions) {
		this.numDimensions = numDimensions;
	}
	public int getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(int caseNumber) {
		this.caseNumber = caseNumber;
	}
	public Algorithm getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(Algorithm algorithm) {
		this.algorithm = algorithm;
	}
	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}
	public Classifier getClassifier() {
		return classifier;
	}
	public void setClassifier(Classifier classifier) {
		this.classifier = classifier;
	}
	public Instances getTrainingInstances() {
		return trainingInstances;
	}
	public void setTrainingInstances(Instances trainingInstances) {
		this.trainingInstances = trainingInstances;
	}
	public Instances getTestInstances() {
		return testInstances;
	}
	public void setTestInstances(Instances testInstances) {
		this.testInstances = testInstances;
	}
	public Instances getHiddenInstances() {
		return hiddenInstances;
	}
	public void setHiddenInstances(Instances hiddenInstances) {
		this.hiddenInstances = hiddenInstances;
	}
	public Evaluation getTrainingModelEvaluation() {
		return trainingModelEvaluation;
	}
	public void setTrainingModelEvaluation(Evaluation trainingModelEvaluation) {
		this.trainingModelEvaluation = trainingModelEvaluation;
	}
	public Evaluation getTestModelEvaluation() {
		return testModelEvaluation;
	}
	public void setTestModelEvaluation(Evaluation testModelEvaluation) {
		this.testModelEvaluation = testModelEvaluation;
	}
	public Evaluation getHiddenModelEvaluation() {
		return hiddenModelEvaluation;
	}
	public void setHiddenModelEvaluation(Evaluation hiddenModelEvaluation) {
		this.hiddenModelEvaluation = hiddenModelEvaluation;
	}

	@Override
	public int compareTo(EpochResultHolder o) {
		return (int) (o.getTestModelEvaluation().correct() - this.testModelEvaluation.correct());
	}

	public Double[] getHiddenPredictions() {
		return hiddenPredictions;
	}

	public void setHiddenPredictions(Double[] hiddenPredictions) {
		this.hiddenPredictions = hiddenPredictions;
	}

	public Double[] getHiddenErrors() {
		return hiddenErrors;
	}

	public void setHiddenErrors(Double[] hiddenErrors) {
		this.hiddenErrors = hiddenErrors;
	}

	public Double[] getCorrectAnswers() {
		return correctAnswers;
	}

	public void setCorrectAnswers(Double[] correctAnswers) {
		this.correctAnswers = correctAnswers;
	}
	
	
	
}

