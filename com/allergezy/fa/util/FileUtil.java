package com.allergezy.fa.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * This is a utility file that enables easy read-write to a file
 * 
 * @author Ayush Alag
 *
 */
public class FileUtil implements FoodAllergyConstants {

	/**
	 * Get a Buffered writer to the specified file
	 * 
	 * @param fileName
	 * @return
	 */
	public static BufferedWriter getBufferedWriter(String fileName) {
		try {
			return new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName), "UTF-8"));
		} catch (Throwable th) {
			th.printStackTrace();
		}
		return null;
	}

	/**
	 * Get a buffered reader to the specified file
	 * 
	 * @param fileName
	 * @return
	 */
	public static BufferedReader getBufferedReader(String fileName) {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), "UTF8"));
			return in;
		} catch (Throwable th) {
			th.printStackTrace();
		}
		return null;
	}

	/**
	 * Get the rows to allergy samples
	 * 
	 * @return
	 */
	public static String[] readAllergyRowsFromFile() {
		return readContentFromFile(GSE59999AllergyFile);
	}

	/**
	 * Get the rows to the sensitized samples
	 * 
	 * @return
	 */
	public static String[] readSenRowsFromFile() {
		return readContentFromFile(GSE59999SenFile);
	}
	
	/**
	 * Read the non-allergic rows
	 * @return
	 */
	public static String[] readNonAllergicRowsFromFile() {
		return readContentFromFile(GSE59999NonAllergicFile);
	}

	/**
	 * Utilily method to read contents from a file
	 * 
	 * @param fileName
	 * @return
	 */
	private static String[] readContentFromFile(String fileName) {
		return InMemoryFileReader.getFileContentByRows(fileName);
	}
	
	/**
	 * Test for reading samples
	 */
	public static void main(String [] args) {
		System.out.println("Current path=" + new File("").getAbsoluteFile());
		
		String allergyKey = getFileDataForTesting(FileUtil.readAllergyRowsFromFile(), "Allergy file has .. " );
		String senKey = getFileDataForTesting(FileUtil.readSenRowsFromFile(), "Sensitized file has .. " );
		String nonAllergicKey = getFileDataForTesting(FileUtil.readNonAllergicRowsFromFile(), "Non Allergy file has .. " );
		
		System.out.println("Allergy and Sensitized column match = " + (allergyKey.compareTo(senKey)==0));
		System.out.println("Allergy and Non-allergic column match = " + (allergyKey.compareTo(nonAllergicKey)==0));
		
	}
	
	/**
	 * helper method
	 */
	private static String getFileDataForTesting(String [] rows, String text) {
		System.out.println("Allergy file has .. " + rows.length + " rows.");
		String firstRow = rows[0];
		String [] headers = firstRow.split("\t");
		System.out.println("Number of columns = " + headers.length);
		String key = headers[0] + "\t" + headers[1] + "\t..\t" + headers[headers.length-1];
		System.out.println(key);
		return key;
	}
}
