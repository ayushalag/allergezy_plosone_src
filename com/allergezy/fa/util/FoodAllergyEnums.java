package com.allergezy.fa.util;


/**
 * This file contains enums that are used in this project
 * 
 * @author Ayush Alag
 *
 */
public interface FoodAllergyEnums {

	/**
	 * Classification enum with value to be used in classification
	 * 
	 */
	public static enum Classification {
		Allergy("Allergy", 1), Sensitized("Sensitized", 0);
		private int value = 0;
		private String name = "";

		Classification(String name, int value) {
			this.name = name;
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}

		public String getName() {
			return this.name;
		}

		public static Classification getClassification(int value) {
			for (Classification c : Classification.values()) {
				if (c.getValue() == value) {
					return c;
				}
			}
			return null;
		}
	}

	// These strings are used to read/write data about each case consistently
	public static final String TRAINING_KEY = "Training:\t";
	public static final String TEST_KEY = "Test:\t";
	public static final String HIDDEN_KEY = "Hidden:\t";
	public static final String NORMAL_KEY = "Normal:\t";

	/**
	 * Enum to define what kind of data -- training, test, or hidden is to be
	 * retrieved
	 *
	 */
	public enum DataType {
		TRAIN("Train", TRAINING_KEY), 
		TEST("Test", TEST_KEY), 
		HIDDEN("Hidden", HIDDEN_KEY), 
		NORMAL("Normal",NORMAL_KEY);

		private String key = null;
		private String name = null;

		DataType(String name, String key) {
			this.name = name;
			this.key = key;
		}

		public String getKey(int caseNumber) {
			return this.key + caseNumber;
		}

		public String getName() {
			return this.name;
		}

	}

	/**
	 * The different kinds of machine-learning algorithms that are used
	 *
	 */
	public enum Algorithm {
		MLP(), RBF(), LINEAR_REGRESSION, LOGISTIC_REGRESSION, DECISION_TREE, SVM;

		/**
		 * This method returns the Algorithm enum based on the String text
		 * passed in
		 * 
		 * @param a
		 * @return
		 */
		public static Algorithm getAlgorithm(String a) {
			for (Algorithm algorithm : Algorithm.values()) {
				if (a.equals(algorithm.name())) {
					return algorithm;
				}
			}
			return null;
		}
	};
}
