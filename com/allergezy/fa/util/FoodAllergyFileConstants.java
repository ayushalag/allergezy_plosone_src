package com.allergezy.fa.util;

import java.io.File;

/**
 * This interface contains locations to files that are read and written to in
 * this project.
 * 
 * @author Ayush Alag
 *
 */
public interface FoodAllergyFileConstants extends FoodAllergyEnums {
	public static final String BasePath = "data" + File.separator;
	public static final String BasePathInput = BasePath + File.separator + "input" + File.separator + "gse59999"
			+ File.separator;
	public static final String BasePathOutput = BasePath + File.separator + "output" + File.separator;

	// Input files with raw CpG methylation data
	// 215MB files with allergic samples
	public static final String GSE59999AllergyFile = BasePathInput + "allergy-59999-datasets.txt";
	// 215MB file with sensitized samples
	public static final String GSE59999SenFile = BasePathInput + "sensitized-59999-datasets.txt";
	// 98.9MB file with non-allergic samples
	public static final String GSE59999NonAllergicFile = BasePathInput + "nonallergic-59999-datasets.txt";

	// 40-10-8 GSE59999
	/**
	 * This file contains the list of samples that are hidden and for each of
	 * the 8 cases which are used training and testing.
	 */
	public static final String GSE59999_40_10_8_DataFile = BasePathInput + "k401008" + File.separator
			+ "gse59999-kfold-cases.txt";
	// Final version used in the study
	public static final String GSE59999_40_10_8_DataFile_Read = BasePathInput + "k401008" + File.separator
			+ "gse59999-kfold-cases-final.txt";
	
	// Geo2RCpgs
	//File with differential CPGs for the 8 cases. 8 cols x 99 rows
	public static final String GSE59999_40_10_8_Geo2R_cpg = BasePathInput + "geo2r" + File.separator + "Geo2R-cpgs.txt";
	//Statistics extracted from analyzing the 8 x 99 CpGs from Geo2R
	public static final String GSE59999_40_10_8_FeatureScore_Geo2R = BasePathInput + "geo2r" + File.separator
			+ "geo2RfeatureScoring.txt";
	//Full GSE59999 annotations from GEO2R. This is used to map CPGs to Genes
	public static final String GSE59999_Geo2R_to_CPG_Annotations = BasePathInput + "geo2r" + File.separator
			+ "Geo2r-gse5999-full-sign.txt";
	
	//Create the ML Dataset with the filtered list of CpGs
	// Master file for cpg values for each of the cases
	public static final String GSE59999_40_10_8_CPG_Data = BasePathInput + "ml-data" + File.separator + "case-";
	public static final String GSE59999_40_10_8_NonAllergic_CPG_Data = BasePathInput + 
			"ml-data" + File.separator + "case--nonAllergic.txt";
	
	//Single feature case files
	//Detailed result for each case
	public static final String GSE59999_40_10_8_Single_Feature_Detail_Result = BasePathOutput + "singlefeature"
				+ File.separator + "kfoldresult" + File.separator ;
	//Feature scoring
	public static final String GSE59999_40_10_8_Single_Feature_Score_Result = BasePathOutput + "singlefeature" 
					+ File.separator + "kfoldresult" + File.separator + "scores.txt";
	//CPG scores across epochs
	public static final String GSE59999_40_10_8_BestFeatureScore_CPG_Result = BasePathOutput + "singlefeature" 
			+ File.separator + "kfoldresult" + File.separator +  "bestFeatureScoresCpg.txt";
	//CPG scores per epoch and feature
	public static final String GSE59999_40_10_8_BestFeatureScore_CPGEpoch_Result = BasePathOutput + "singlefeature" 
			+ File.separator + "kfoldresult" + File.separator + "bestFeatureScoresCpgEpoch.txt";
	
	//Multi-Feature cases
	public static final String GSE59999_Verify_CPG_Cases_Gen = BasePathOutput + "multifeature" + File.separator +
			"setup" + File.separator + "verifyCpgCases.txt";
	/**
	 * Verify files
	 */
	public static final String GSE59999_Verify_Cases_InputFileName = BasePathOutput + "multifeature" + File.separator +
			"setup" + File.separator + "summary.txt" ;
	public static final String GSE59999_Verify_DetailCombination_Result = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "details" + File.separator;
	public static final String GSE59999_Verify_BestAnalyzerOut_CaseResult = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "votingPredictions.txt";
	public static final String GSE59999_Verify_FeatureCombination_Result = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator +  "allCasesScores.txt";
	public static final String GSE59999_Verify_NonAllergic_Result = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator +  "nonAllergicScores.txt";
	//Analysis
	public static final String GSE59999_40_10_8_FeatureCombination_CPGResult = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "best-CaseoverAllEpochs.txt";
	public static final String GSE59999_40_10_8_FeatureCombination_CaseResult = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "best-CaseEachEpoch.txt";

	
	
	//Raw data extractor
	public static final String GSE59999_Raw_CPG_Data = BasePathOutput + "multifeature" + File.separator +
			"setup" + File.separator + "rawCpgData.txt";
	//Feature analyzer -- final across all CpGs of interest
	public static final String GSE59999_Verify_FeatureAnalyzer_CaseResult = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "analyze" + File.separator  + "analyze-features.txt";
	//CPG gene annotator
	public static final String GSE59999_CPG_GeneAnnotation = BasePathOutput + "multifeature" + File.separator +
			"result" + File.separator + "analyze" + File.separator   + "genes.txt";
	
	//Simulation related files
	//All sample ids
	public static final String GSE59999_ALL_SAMPLE_IDs = BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "sampleids.txt";
	public static final String GSE59999_CPG_IDs = BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "cpgids.txt";
	public static final String GSE59999_Dataset_File= BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "dataset-18cpg.txt";
	public static final String CPG_SIGN_File = BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "summary.txt";
	public static final String SIMUL_OUT_File = BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "result" + File.separator + "simulOutput";
	public static final String SUMM_SIMUL_OUT_File = BasePathOutput + "simulation" + File.separator + "dataset"
			+ File.separator + "result" + File.separator + "summarySimulation.txt";
	
	
}
