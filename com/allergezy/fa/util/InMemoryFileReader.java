package com.allergezy.fa.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * This is a utility class to read and write content from a file. 
 * Note this will read the content from the file into memory.
 * 
 * @author Ayush Alag
 *
 */
public class InMemoryFileReader implements FoodAllergyConstants {
	/**
	 * Retrieve the contents of a file
	 * @param fileName the File to be read
	 * @return
	 */
	public static String readFileContent(String fileName) {
		return readFileContent(fileName,"");
	}
	
	/**
	 * Retrieves the row-by-row content of a file
	 * @param fileName he File to be read
	 * @return
	 */
	public static String[] getFileContentByRows(String fileName) {
		String delimiter = "---";
		String content = readFileContent(fileName,delimiter);
		return content.split(delimiter);
	}
	
	/**
	 * Retrieves the content of a file
	 * 
	 * @param fileName
	 * @param lineBreak
	 * @return
	 */
	private static String readFileContent(String fileName,String lineBreak) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(
					   new InputStreamReader(
			                      new FileInputStream(fileName), "UTF8"));
			
			String strLine;
			// Read File Line By Line
			int count = 0;
		
			while ((strLine = br.readLine()) != null) {
				sb.append(strLine+lineBreak);
				count++;
				if (count % 300000 == 0) {
					System.out.println(count + " " + strLine);
				}
			}
			
			System.out.println("InMemoryFileReader::Total number of lines " + fileName + " =" + count);
			br.close();
		} catch (Throwable th) {
			th.printStackTrace();
			System.out.println(th.getMessage());
		}
		return sb.toString();
	}
}
