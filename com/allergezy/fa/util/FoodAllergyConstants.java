package com.allergezy.fa.util;

import java.io.File;

/**
 * This interface contains the list of constants that are used in this project.
 * 
 * @author Ayush Alag
 *
 */
public interface FoodAllergyConstants extends FoodAllergyFileConstants {
	

}
